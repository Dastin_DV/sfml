#pragma once

#include "SFML/Graphics.hpp"
#include "Helper.h"
#include "Settings.h"
#include "Button.h"

class GameOverScreen {
public:
	GameOverScreen(sf::RenderWindow& window) {
		auto pathToHUD = Settings::getInstance()->getHUDByGameState(GameState::GameOver);
		auto pathToFont = Settings::getInstance()->getPathToFont();
		loadImgAndCreateTexture(pathToHUD, textureBack);
		loadFontAndCreateText(pathToFont, font, text, 16);
		spriteBack.setTexture(textureBack);
		text.setString("Game over!");
		init(window);
		initResetButton(window);
		initExitButton(window);
	}

	void update(sf::RenderWindow& window, sf::Event& event);
	void draw(sf::RenderWindow& window);
	void initResetButton(sf::RenderWindow& window);
	void initExitButton(sf::RenderWindow& window);
	void init(sf::RenderWindow& window);
	void clearButtonsStates();

private:

	sf::Texture textureBack;
	sf::Sprite spriteBack;
	sf::Font font;
	sf::Text text;
	myGui::button tryAgainButton;
	myGui::button exitButton;
	const float ButtonYOffset = 10.f;

	float scaleFactor = 2;
};