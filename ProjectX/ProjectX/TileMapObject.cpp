#include <iostream>

#include "TileMapObject.h"
#include "Helper.h"
#include "Settings.h"

void TileMapObject::loadTexture() {
	auto pathToImg = Settings::getInstance()->getAssetPathByObjName(this->name);
	loadImgAndCreateTexture(pathToImg, texture);
}

void TileMapObject::draw(sf::RenderWindow& window) {
	if (sprite.getTexture()->getSize().x != 0)
		window.draw(sprite);
}

std::pair<int, int> TileMapObject::getTilePosByWorldCoordinates(sf::Vector2f neigbourPos) {
	int xWorldPos = neigbourPos.x / 16;
	int yWorldPos = neigbourPos.y / 16;

	std::cout << xWorldPos << " " << yWorldPos << " " << (*grid)[xWorldPos][yWorldPos] << std::endl;
	return { xWorldPos, yWorldPos };
}

bool TileMapObject::isNeighbourTileWalkable(sf::Vector2f neigbourPos) {
	auto pos = getTilePosByWorldCoordinates(neigbourPos);
	if (!walkableId.count((*grid)[pos.first][pos.second]))
		return false;

	return true;
}

bool TileMapObject::isLineTileWalkable(sf::Vector2f from, sf::Vector2f to) {
	auto posFrom = getTilePosByWorldCoordinates(from);
	auto posTo = getTilePosByWorldCoordinates(to);

	if (posFrom.first == posTo.first) {				// Horizontal Line
		int minPosY = posFrom.second <= posTo.second ? posFrom.second : posTo.second;
		int maxPosY = posFrom.second >= posTo.second ? posFrom.second : posTo.second;
		for (minPosY; minPosY != maxPosY; minPosY++) {
			if (!walkableId.count((*grid)[posFrom.first][minPosY]))
				return false;
		}
	} else if (posFrom.second == posTo.second) {	// Vertical Line
		int minPosX = posFrom.first <= posTo.first ? posFrom.first : posTo.first;
		int maxPosX = posFrom.first >= posTo.first ? posFrom.first : posTo.first;
		for (minPosX; minPosX != maxPosX; minPosX++) {
			if (!walkableId.count((*grid)[minPosX][posFrom.second]))
				return false;
		}
	}
		
	return true;
}
