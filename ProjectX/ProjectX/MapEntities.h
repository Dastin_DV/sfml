#pragma once
#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <SFML/Audio.hpp>

#include "SFML/Graphics.hpp"
#include "tinyxml2.h"
#include "TileMapObject.h"
#include "MainHero.h"

struct Tile {
	int gid;
	sf::Sprite sprite;
};

struct TileMapLayer {
	int opacity;
	std::string name;
	std::vector<Tile> tiles;
};

class TileMapLVL {
public:

	/*TileMapLVL() {
		staticObjects = std::make_shared<std::vector<std::shared_ptr<StaticObject>>>();
	}*/

	bool loadLVLfromFile(const std::string fileName);
	void loadBackGroundSound(const std::string& path);
	void draw(sf::RenderWindow& window);
	void playMusic();
	void updateObjects(sf::RenderWindow& window, sf::Event& event);
	void updateObjectsWithDeltaTime(sf::Time deltaTime, sf::RenderWindow& window, sf::Event& event);
	
	bool isLVLPassed(int heroCoins, bool chestFound, bool exitWasFound);

	std::shared_ptr<MainHero> getMainHero() { return mainHero; }
	void setCoinHUDPos(sf::Vector2f coinHUDpos) { this->coinHUDpos = coinHUDpos; }
	void setSwordHUDPos(sf::Vector2f swordHUDPos) { this->swordHUDpos = swordHUDPos; }

	sf::Vector2f getLeftUpCornerPos() { return leftUpCornerPos; }
	int getMapWidth() { return mapWidth; }
	int getMapHeight() { return mapHeight; }
	int getTileWidth() { return tileWidth; }
	int getTileHeight() { return tileHeight; }
	int getMapPixelHeight() { return mapHeight * tileHeight; }
	int getMapPixelWidth() { return mapWidth * tileWidth; }
	int getCoinsCountInLVL() { return coinsInLVL; }
	int getSwordsCountInLVL() { return swordsInLVL; }

private:
	const unsigned FLIPPED_HORIZONTALLY_FLAG = 0x80000000;
	const unsigned FLIPPED_VERTICALLY_FLAG = 0x40000000;
	const unsigned FLIPPED_DIAGONALLY_FLAG = 0x20000000;

	int mapWidth;
	int mapHeight;
	int tileWidth;
	int tileHeight;

	int tilesSetColumns;
	int tileSetRows;
	int firstTileId;

	int coinsInLVL;
	int swordsInLVL;

	std::string tilesetImgPath;
	sf::Texture tilesetTexture;								// �������� �����
	
	sf::Font font;
	sf::Vector2f leftUpCornerPos = { 0.0f,0.0f };
	sf::Vector2f coinHUDpos;
	sf::Vector2f swordHUDpos;

	std::vector<TileMapLayer> layers;
	std::vector<std::shared_ptr<TileMapObject>> objects;
	std::shared_ptr<std::vector<std::shared_ptr<StaticObject>>> staticObjects;
	std::shared_ptr<std::vector<std::shared_ptr<StaticObject>>> hudObjects;
	std::shared_ptr<std::vector<std::vector<int>>> mapGrid;
	std::shared_ptr<MainHero> mainHero;

	sf::SoundBuffer backGroundBuffer;
	sf::Sound backGroundSound;								// ��� �������� ������.

	void parseLayers(tinyxml2::XMLElement* map);
	void parseLayer(tinyxml2::XMLElement* currentLayer);
	void parseObjects(tinyxml2::XMLElement* map);
	void parseObject(tinyxml2::XMLElement* currentObjectGroup);
	void spawnObjects();
	void removeObjectById(int id);
	void determineGlobalID(int& id);						// ����������� ����� ������� ����� � �������� ���������� ����.
	void loadFont();

	std::vector<std::shared_ptr<TileMapObject>>& getObjects() { return objects; };
};