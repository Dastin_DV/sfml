#pragma once

#include "SFML/Graphics.hpp"
#include <SFML/Audio.hpp>
#include "Helper.h"
#include "Settings.h"
#include "Button.h"

class MainMenuScreen {

public:
	
	MainMenuScreen(sf::RenderWindow& window) {
		auto pathToHUD = Settings::getInstance()->getHUDByGameState(GameState::Menu);
		auto pathToFont = Settings::getInstance()->getPathToFont();
		auto pathToSound = Settings::getInstance()->getSoundByName("MainMenu");

		if (!soundBuffer.loadFromFile(pathToSound))
			throw std::runtime_error("Sound not found");

		sound.setBuffer(soundBuffer);
		sound.setLoop(true);

		loadImgAndCreateTexture(pathToHUD, textureBack);
		spriteBack.setTexture(textureBack);
		loadFontAndCreateText(pathToFont, font, text, 16);
		init(window);

		sound.play();
		//initPlayButton(window);
		//initExitButton(window);
	}

	void update(sf::RenderWindow& window, sf::Event& event);
	void draw(sf::RenderWindow& window);
	void initPlayButton(sf::RenderWindow& window);
	void initSelectLVLButton(sf::RenderWindow& window);
	void initExitButton(sf::RenderWindow& window);
	void init(sf::RenderWindow& window);
	void clearButtonsStates();
	void playMusic();

private:
	sf::Texture textureBack;
	sf::Sprite spriteBack;
	sf::Font font;
	sf::Text text;
	myGui::button playButton;
	myGui::button choseLVLButton;
	myGui::button exitButton;

	sf::SoundBuffer soundBuffer;
	sf::Sound sound;

	float scaleX = 0;
	float scaleY = 0;
	const float ButtonYOffset = 15.f;
};