#include "Enemy.h"
#include <iostream>

void Enemy::update(sf::Event& event) {

	auto enemyPos = this->sprite.getPosition();

	if (!isActive) {
		if (((enemyPos.x - 32 == mainHeroPos.x && enemyPos.y == mainHeroPos.y)
			&& isNeighbourTileWalkable({ enemyPos.x - 16, enemyPos.y }))) {
			isActive = true;
			sprite.setPosition(sprite.getPosition().x - 16, sprite.getPosition().y);
		}
		if ((enemyPos.x + 32 == mainHeroPos.x && enemyPos.y == mainHeroPos.y
			&& isNeighbourTileWalkable({ enemyPos.x + 16, enemyPos.y }))) {
			isActive = true;
			sprite.setPosition(sprite.getPosition().x + 16, sprite.getPosition().y);
		}
		if (((enemyPos.x == mainHeroPos.x && enemyPos.y - 32 == mainHeroPos.y)
			&& isNeighbourTileWalkable({ enemyPos.x, enemyPos.y - 16 }))) {
			isActive = true;
			sprite.setPosition(sprite.getPosition().x, sprite.getPosition().y - 16);
		}
		if ((enemyPos.x == mainHeroPos.x && enemyPos.y + 32 == mainHeroPos.y
			&& isNeighbourTileWalkable({ enemyPos.x, enemyPos.y + 16 }))) {
			isActive = true;
			sprite.setPosition(sprite.getPosition().x, sprite.getPosition().y + 16);
		}
	} else {
		sprite.setPosition(mainHeroPos.x, mainHeroPos.y);
	}
};

void Enemy::setFlagsTofalse() {
	goLeft = false;
	goRight = false;
	goDown = false;
	goUp = false;
}

void FastEnemy::update(sf::Event& event) {

	auto enemyPos = this->sprite.getPosition();

	if (enemyPos.y == mainHeroPos.y && isLineTileWalkable(enemyPos, mainHeroPos)) {
		if (enemyPos.x > mainHeroPos.x) {
			goLeft = true;
		}
		else {
			goRight = true;
		}
	}
	if (enemyPos.x == mainHeroPos.x && isLineTileWalkable(enemyPos, mainHeroPos)) {
		if (enemyPos.y > mainHeroPos.y) {
			goDown = true;
		}
		else {
			goUp = true;
		}
	}
	if (enemyPos.y == prevMainHeroPos.y && isLineTileWalkable(enemyPos, prevMainHeroPos)) {
		if (enemyPos.x > prevMainHeroPos.x) {
			goLeft = true;
		}
		else {
			goRight = true;
		}
	}
	if (enemyPos.x == prevMainHeroPos.x && isLineTileWalkable(enemyPos, prevMainHeroPos)) {
		if (enemyPos.y > prevMainHeroPos.y) {
			goDown = true;
		}
		else {
			goUp = true;
		}
	}

	makeStepsTowardAim();
	setFlagsTofalse();
}

void FastEnemy::makeStepsTowardAim() {
	for (int i = 0; i < stepCount; i++) {
		if (goLeft && !isCatchedMainHero())
			sprite.setPosition(sprite.getPosition().x - 16, sprite.getPosition().y);
		else if (goRight && !isCatchedMainHero())
			sprite.setPosition(sprite.getPosition().x + 16, sprite.getPosition().y);
		else if (goUp && !isCatchedMainHero())
			sprite.setPosition(sprite.getPosition().x, sprite.getPosition().y + 16);
		else if (goDown && !isCatchedMainHero())
			sprite.setPosition(sprite.getPosition().x, sprite.getPosition().y - 16);
		checkCollisionWithObject();
	}
}

bool FastEnemy::isCatchedMainHero() {
	if (this->sprite.getPosition() == mainHeroPos)
		return true;

	return false;
}

void FastEnemy::checkCollisionWithObject() {
	for (auto& otherObject : objects) {
		if (sprite.getGlobalBounds().intersects(otherObject->sprite.getGlobalBounds())) {
			if (otherObject->name == "Enemy") {
				otherObject->isAlive = false;
				killSound.play();
				std::shared_ptr<StaticObject> deadEnemy = std::make_shared<StaticObject>();
				deadEnemy->initObject(Settings::getInstance()->getAssetPathByObjName("EnemyDead"), otherObject->getSprite().getPosition());
				staticObjects->push_back(deadEnemy);
				std::cout << "Fast Kills just enemy!" << std::endl;
			}
		}
	}
}