#include "StaticObject.h"
#include "Helper.h"

void StaticObject::initObject(std::string pathToTexture) {
	loadImgAndCreateTexture(pathToTexture, texture);
	this->sprite.setTexture(texture);
}

void StaticObject::initObject(std::string pathToTexture, sf::Vector2f pos) {
	loadImgAndCreateTexture(pathToTexture, texture);
	this->sprite.setTexture(texture);
	this->sprite.setPosition(pos);
}