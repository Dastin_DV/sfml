#pragma once
#include <exception>
#include <string>

#include "tinyxml2.h"

class ParsingException : public std::exception {

public:
	ParsingException(tinyxml2::XMLError xmlIdError) {
		this->idError = xmlIdError;
	}

	char const* what() const override {
		std::string answer;
		switch (idError) {
			case tinyxml2::XMLError::XML_ERROR_FILE_NOT_FOUND: {
				answer += "Xml file not found! Check the existance of a file";
			}
			case tinyxml2::XMLError::XML_ERROR_FILE_COULD_NOT_BE_OPENED: {
				answer += "Xml file could't be opened!";
			}
			default: {
				answer += "Not recognized or unimplemented error while parsing";
			}
		}
		return answer.c_str();
	}

private:
	tinyxml2::XMLError idError;
};
