#include <iostream>

#include <SFML/Graphics.hpp>
#include "MapEntities.h"
#include "Exceptions.h"
#include "Settings.h"
#include "GameWorld.h"

const sf::Time TimePerFrame = sf::seconds(1.f / 60.f);

int main()
{
    sf::RenderWindow window(sf::VideoMode(640, 480), "SFML works!");
    sf::CircleShape shape(100.f);
    shape.setFillColor(sf::Color::Green);

    try {
        GameWorld gameWorld(window);
        //gameWorld.setLVLIt(prev(gameWorld.getLvls().end()));
        gameWorld.setNextLVL();
        Settings::getInstance()->setGameState(GameState::Menu);
        sf::Clock clock;
        sf::Time timeSinceLastUpdate = sf::Time::Zero;

        while (window.isOpen())
        {
            timeSinceLastUpdate += clock.restart();
            sf::Event event;
            while (window.pollEvent(event))
            {
                if (event.type == sf::Event::Closed)
                    window.close();
                gameWorld.update(window, event);
            }

            while (timeSinceLastUpdate > TimePerFrame)
            {
                    window.clear();
                    timeSinceLastUpdate -= TimePerFrame;
                    gameWorld.update(TimePerFrame, window, event);
                    gameWorld.draw(window);
                    window.display();
            }
            //}
        }
    }
    catch (std::runtime_error err) {
        std::cout << err.what() << std::endl;
    }
    catch (ParsingException exc) {
        std::cout << exc.what() << std::endl;
    }

    return 0;
}