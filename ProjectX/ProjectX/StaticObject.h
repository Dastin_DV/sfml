#pragma once
#include "SFML/Graphics.hpp"

// ������ ��������� ������, ������� ����� �������� �� �� ����� ����������� ��������������.
class StaticObject {
public:

	void initObject(std::string pathToTexture);
	void initObject(std::string pathToTexture, sf::Vector2f pos);

	sf::Sprite& getSprite() { return sprite; }
	void setPosition(sf::Vector2f pos) { this->sprite.setPosition(pos); }
	void setName(std::string name) { this->name = name; }
	std::string getName() { return this->name; }
	void setIsAlive(bool alive) { this->alive = alive; }
	bool isAlive() { return alive; }

private:
	std::string name;
	sf::Texture texture;
	sf::Sprite sprite;
	sf::Vector2f position;
	bool alive = true;
};