#pragma once
#include "MapEntities.h"

class Enemy : public TileMapObject {

public:
	void update(sf::Event& event) override;
	void setMainHeroPos(sf::Vector2f& mainHeroPos) { this->mainHeroPos = mainHeroPos; };
	bool isActiveEnemy() { return isActive; };
	void checkCollisionWithObject(std::shared_ptr<TileMapObject>& object) override {};

protected:

	void setFlagsTofalse();
	bool isActive = false;
	bool goUp = false;
	bool goLeft = false;
	bool goDown = false;
	bool goRight = false;
	sf::Vector2f mainHeroPos;
};

class FastEnemy : public Enemy {
public:

	FastEnemy() {
		if (!killBuffer.loadFromFile(Settings::getInstance()->getSoundByName("Enemy")))
			throw std::runtime_error("Sound not found");

		killSound.setBuffer(killBuffer);
	}

	void update(sf::Event& event) override;
	void setPrevMainHeroPos(sf::Vector2f& prevMainHeroPos) { this->prevMainHeroPos = prevMainHeroPos; };
	void setObjects(std::vector<std::shared_ptr<TileMapObject>>& objects) { this->objects = objects; }
	void setStaticObjects(std::shared_ptr<std::vector<std::shared_ptr<StaticObject>>> staticObjects) { this->staticObjects = staticObjects; }
	void checkCollisionWithObject();

private:
	const int stepCount = 2;
	void makeStepsTowardAim();
	bool isCatchedMainHero();
	sf::Vector2f prevMainHeroPos;
	std::vector<std::shared_ptr<TileMapObject>> objects;
	std::shared_ptr<std::vector<std::shared_ptr<StaticObject>>> staticObjects;

	sf::SoundBuffer killBuffer;
	sf::Sound killSound;
};