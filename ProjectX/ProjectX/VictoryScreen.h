#pragma once
#include "SFML/Graphics.hpp"
#include <SFML/Audio.hpp>

#include "Helper.h"
#include "Settings.h"
#include "Button.h"

class VictoryScreen {

public:
	VictoryScreen(sf::RenderWindow& window) {
		auto pathToHUD = Settings::getInstance()->getHUDByGameState(GameState::Victory);
		auto pathToFont = Settings::getInstance()->getPathToFont();

		loadImgAndCreateTexture(pathToHUD, textureBack);
		spriteBack.setTexture(textureBack);
		loadFontAndCreateText(pathToFont, font, text, 16);
		text.setString("Victory!!!");

		init(window);
	}

	void update(sf::RenderWindow& window, sf::Event& event);
	void draw(sf::RenderWindow& window);
	void init(sf::RenderWindow& window);
	void initSelectLVLButton(sf::RenderWindow& window);
	void initExitButton(sf::RenderWindow& window);

private:
	sf::Texture textureBack;
	sf::Sprite spriteBack;
	sf::Font font;
	sf::Text text;
	myGui::button choseLVLButton;
	myGui::button exitButton;

	const float ButtonYOffset = 15.f;
};