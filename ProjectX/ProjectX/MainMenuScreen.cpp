#include "MainMenuScreen.h"

void MainMenuScreen::update(sf::RenderWindow& window, sf::Event& event) {
	playButton.update(event, window);
	if (playButton.getState() == myGui::state::clicked) {
		Settings::getInstance()->setGameState(GameState::Game);
	}

	choseLVLButton.update(event, window);
	if (choseLVLButton.getState() == myGui::state::clicked) {
		Settings::getInstance()->setGameState(GameState::LevelSelection);
	}

	exitButton.update(event, window);
	if (exitButton.getState() == myGui::state::clicked) {
		Settings::getInstance()->setGameState(GameState::Exit);
	}
}

void MainMenuScreen::draw(sf::RenderWindow& window) {
	window.draw(spriteBack);
	window.draw(playButton);
	window.draw(choseLVLButton);
	window.draw(exitButton);
}

void MainMenuScreen::init(sf::RenderWindow& window) {
	auto view = window.getView();
	auto targetSize = view.getSize();
	float scaleX = targetSize.x / spriteBack.getLocalBounds().width;
	float scaleY = targetSize.y / spriteBack.getLocalBounds().height;

	view.setCenter(0 + spriteBack.getLocalBounds().width / 2 * scaleX,
				   0 + spriteBack.getLocalBounds().height / 2 * scaleY);

	//std::cout << "ScreenCenter x: " << view.getCenter().x << " y: " << view.getCenter().y << std::endl;
	//std::cout << "Scale x: " << scaleX << " y: " << scaleY << std::endl;

	spriteBack.setScale(scaleX ,scaleY);

	//std::cout << "Background position x: " << spriteBack.getPosition().x << " y: " << spriteBack.getPosition().y << std::endl;
	
	window.setView(view);
	initPlayButton(window);
	initExitButton(window);
	initSelectLVLButton(window);
	clearButtonsStates();
}

void MainMenuScreen::initPlayButton(sf::RenderWindow& window) {
	myGui::button button;
	button.setText("Play");
	button.setFont(font);
	button.setStyle(myGui::style::save);
	button.setBorderRadius(1.f);
	button.setBorderThickness(1.f);
	button.setSize(12);
	button.setPosition({ window.getView().getCenter().x, window.getView().getCenter().y });
	
	//std::cout << "PlayButton position x: " << button.getPosition().x << " y: " << button.getPosition().y << std::endl;
	playButton = std::move(button);
}

void MainMenuScreen::initSelectLVLButton(sf::RenderWindow& window) {
	myGui::button button;
	button.setText("Chose LVL");
	button.setFont(font);
	button.setStyle(myGui::style::save);
	button.setBorderRadius(1.f);
	button.setBorderThickness(1.f);
	button.setSize(12);
	button.setPosition({ window.getView().getCenter().x, window.getView().getCenter().y + ButtonYOffset });

	choseLVLButton = button;
}

void MainMenuScreen::initExitButton(sf::RenderWindow& window) {
	myGui::button button;
	button.setText("Exit");
	button.setFont(font);
	button.setStyle(myGui::style::save);
	button.setBorderRadius(1.f);
	button.setBorderThickness(1.f);
	button.setSize(playButton.getDimensions(), 12);
	button.setPosition({ window.getView().getCenter().x, window.getView().getCenter().y + 2 * ButtonYOffset });

	//std::cout << "ExitButton position x: " << button.getPosition().x << " y: " << button.getPosition().y << std::endl;
	exitButton = std::move(button);
}

void MainMenuScreen::clearButtonsStates() {
	playButton.clearButtonState();
	exitButton.clearButtonState();
}

void MainMenuScreen::playMusic() {
	if (Settings::getInstance()->getGameState() != GameState::Menu)
		sound.stop();
}
