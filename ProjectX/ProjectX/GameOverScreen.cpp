#include "GameOverScreen.h"
#include "Settings.h"

void GameOverScreen::update(sf::RenderWindow& window, sf::Event& event) {
	tryAgainButton.update(event, window);
	if (tryAgainButton.getState() == myGui::state::clicked) {
		Settings::getInstance()->setGameState(GameState::Game);
	}
	exitButton.update(event, window);
	if (exitButton.getState() == myGui::state::clicked) {
		Settings::getInstance()->setGameState(GameState::Exit);
	}
}

void GameOverScreen::draw(sf::RenderWindow& window) {
	text.setPosition(window.getView().getCenter().x - text.getLocalBounds().width / 2, window.getView().getCenter().y - window.getView().getSize().y / 2);
	spriteBack.setScale({ 2,2 });
	window.draw(spriteBack);
	window.draw(text);
	window.draw(tryAgainButton);
	window.draw(exitButton);
}

void GameOverScreen::init(sf::RenderWindow& window) {
	auto view = window.getView();
	view.setCenter(0 + spriteBack.getLocalBounds().width / 2 * scaleFactor, 0 + spriteBack.getLocalBounds().height / 2 * scaleFactor);
	window.setView(view);
	clearButtonsStates();
}

void GameOverScreen::initResetButton(sf::RenderWindow& window) {
	myGui::button button;
	button.setText("Try again!");
	button.setFont(font);
	button.setStyle(myGui::style::save);
	button.setBorderRadius(1.f);
	button.setBorderThickness(1.f);
	button.setColorNormal(sf::Color(200, 0, 200, 255));
	button.setColorHover(sf::Color(255, 0, 255, 100));
	button.setColorClicked(sf::Color(150, 0, 150, 255));
	button.setColorTextNormal(sf::Color(255, 255, 255, 255));
	button.setColorTextHover(sf::Color(255, 255, 0, 255));
	button.setColorTextClicked(sf::Color(255, 0, 0, 255));
	button.setSize(9);
	button.setPosition({ window.getView().getCenter().x, window.getView().getCenter().y});
	tryAgainButton = std::move(button);
}

void GameOverScreen::initExitButton(sf::RenderWindow& window) {
	myGui::button button;
	button.setText("Exit!");
	button.setFont(font);
	button.setStyle(myGui::style::save);
	button.setBorderRadius(1.f);
	button.setBorderThickness(1.f);
	button.setColorNormal(sf::Color(200, 0, 200, 255));
	button.setColorHover(sf::Color(255, 0, 255, 100));
	button.setColorClicked(sf::Color(150, 0, 150, 255));
	button.setColorTextNormal(sf::Color(255, 255, 255, 255));
	button.setColorTextHover(sf::Color(255, 255, 0, 255));
	button.setColorTextClicked(sf::Color(255, 0, 0, 255));
	button.setSize(tryAgainButton.getDimensions(), 9);
	button.setPosition({ window.getView().getCenter().x, window.getView().getCenter().y + ButtonYOffset });
	exitButton = std::move(button);
}

void GameOverScreen::clearButtonsStates() {
	tryAgainButton.clearButtonState();
	exitButton.clearButtonState();
}