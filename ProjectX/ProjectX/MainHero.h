#pragma once
#include "TileMapObject.h"
#include "Settings.h"
#include "Helper.h"

#include "SFML/Audio.hpp"

class MainHero : public TileMapObject {

public:
	MainHero() {
		if (!coinBuffer.loadFromFile(Settings::getInstance()->getSoundByName("Coin")))
			throw std::runtime_error("Sound not found");

		coinSound.setBuffer(coinBuffer);

		if (!chestBuffer.loadFromFile(Settings::getInstance()->getSoundByName("Chest")))
			throw std::runtime_error("Sound not found");

		chestSound.setBuffer(chestBuffer);

		if (!swordBuffer.loadFromFile(Settings::getInstance()->getSoundByName("Sword")))
			throw std::runtime_error("Sound not found");

		swordSound.setBuffer(swordBuffer);

		if (!killBuffer.loadFromFile(Settings::getInstance()->getSoundByName("Enemy")))
			throw std::runtime_error("Sound not found");

		killSound.setBuffer(killBuffer);
	}

	void update(sf::Event& event) override;
	void checkCollisionWithObject(std::shared_ptr<TileMapObject>& object) override;
	void reduceSwordCount() { swordCount--; };
	void clearWasEnemyHit() { this->enemyHit = false; }
	bool wasStepMade() { return this->stepMade; }
	bool wasEnemyHit() { return this->enemyHit; }
	int getCoinCount() { return this->coinCount; };
	int getSwordCount() { return this->swordCount; };
	bool chestWasFound() { return this->foundChest; };
	bool exitWasFound() { return this->foundExit; }

	void setStaticObjects(std::shared_ptr<std::vector<std::shared_ptr<StaticObject>>> staticObjects) { this->staticObjects = staticObjects; }
	void setHudObjects(std::shared_ptr<std::vector<std::shared_ptr<StaticObject>>> hudObjects) { this->hudObjects = hudObjects; }

private:

	void spawnObject(std::string objName, sf::Vector2f pos);

	bool stepMade = false;
	bool foundChest = false;
	bool foundExit = false;
	bool enemyHit = false;
	int swordCount = 0;
	int coinCount = 0;

	sf::SoundBuffer coinBuffer;
	sf::Sound coinSound;

	sf::SoundBuffer chestBuffer;
	sf::Sound chestSound;

	sf::SoundBuffer swordBuffer;
	sf::Sound swordSound;

	sf::SoundBuffer killBuffer;
	sf::Sound killSound;

	sf::Texture openChestTexture;

	std::shared_ptr<std::vector<std::shared_ptr<StaticObject>>> staticObjects;
	std::shared_ptr<std::vector<std::shared_ptr<StaticObject>>> hudObjects;
};