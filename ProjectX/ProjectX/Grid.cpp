#include "Grid.h"

void Grid::createGrid() {
	//clearGrid();

	offsetX *= offsetScaleX;			// ��� ��������� ������ ������ - ������������� �������.
	offsetY *= offsetScaleY;
	
	auto objectWidth = (worldSize.x - (offsetX * (col - 1))) / col;
	auto objectHeight = (worldSize.y - (offsetY * (row - 1))) / row;
	origin.x = offsetX + objectWidth / 2;

	sf::Vector2f currentOffset = pos;
	int allObjCount = row + col;
	int objectCounter = 0;
	for (int i = 0; i < row; i++) {
		currentOffset.x = origin.x;
		for (int j = 0; (j < col && objectCounter < allObjCount); j++, objectCounter++) {
			GridObject newObject;
			newObject.width = objectWidth;
			newObject.height = objectHeight;
			newObject.pos.x = currentOffset.x;
			newObject.pos.y = currentOffset.y;
			currentOffset.x += objectWidth + offsetX;
			gridObjects[i][j] = newObject;
		}
		currentOffset.x = pos.x;
		currentOffset.y += objectHeight + offsetY;
	}
}

void Grid::clearGrid() {
	for (auto& row : gridObjects) {
		row.clear();
	}
}

void Grid::resize(float scaleX, float scaleY) {
	offsetScaleX = scaleX;
	offsetScaleY = scaleY;
	createGrid();
}