#include <iostream>

#include "MainHero.h"
#include "SFML/Graphics.hpp"
#include "Settings.h"
#include "Helper.h"
#include "StaticObject.h"

void MainHero::update(sf::Event& event) {
    stepMade = false;
    if (event.type == sf::Event::KeyPressed) {
        if (event.key.code == sf::Keyboard::Left && isNeighbourTileWalkable({ sprite.getPosition().x - 16, sprite.getPosition().y })) {
            sprite.setPosition(sprite.getPosition().x - 16, sprite.getPosition().y);
            stepMade = true;
        }
        if (event.key.code == sf::Keyboard::Right && isNeighbourTileWalkable({ sprite.getPosition().x + 16, sprite.getPosition().y })) {
            sprite.setPosition(sprite.getPosition().x + 16, sprite.getPosition().y);
            stepMade = true;
        }
        if (event.key.code == sf::Keyboard::Up && isNeighbourTileWalkable({ sprite.getPosition().x, sprite.getPosition().y - 16 })) {
            sprite.setPosition(sprite.getPosition().x, sprite.getPosition().y - 16);
            stepMade = true;
        }
        if (event.key.code == sf::Keyboard::Down && isNeighbourTileWalkable({ sprite.getPosition().x, sprite.getPosition().y + 16 })) {
            sprite.setPosition(sprite.getPosition().x, sprite.getPosition().y + 16);
            stepMade = true;
        }
        if (event.key.code == sf::Keyboard::Escape) {
            Settings::getInstance()->setGameState(GameState::Escape);
        }
    }
}

void MainHero::checkCollisionWithObject(std::shared_ptr<TileMapObject>& object) {
    if (sprite.getGlobalBounds().intersects(object->sprite.getGlobalBounds())) {
        if (object->name == "MainHero")
            return;
        if (object->name == "Coin") {
            coinCount++;
            object->isAlive = false;
            coinSound.play();
            spawnObject(object->name, object->getSprite().getPosition());            // For collecting animation.
        }
        if (object->name == "Sword") {
            swordCount++;
            object->isAlive = false;
            swordSound.play();
            spawnObject(object->name, object->getSprite().getPosition());            // For collecting animation.
        }
        if (object->name == "Enemy" || object->name == "FastEnemy") {
            if (swordCount > 0) {
                enemyHit = true;
                object->isAlive = false;
                killSound.play();
                std::shared_ptr<StaticObject> deadEnemy = std::make_shared<StaticObject>();
                if (object->name == "Enemy")
                 deadEnemy->initObject(Settings::getInstance()->getAssetPathByObjName("EnemyDead"), object->getSprite().getPosition());
                else if (object->name == "FastEnemy")
                 deadEnemy->initObject(Settings::getInstance()->getAssetPathByObjName("FastEnemyDead"), object->getSprite().getPosition());
                
                staticObjects->push_back(deadEnemy);
            } else {
                Settings::getInstance()->setGameState(GameState::GameOver);
            }
        }
        if (object->name == "Chest") {
            std::string path = Settings::getInstance()->getAssetPathByObjName("Chest-opened");
            loadImgAndCreateTexture(path, openChestTexture);
            object->sprite.setTexture(openChestTexture);
            chestSound.play();
            foundChest = true;
        }
        if (object->name == "Exit") {
            foundExit = true;
        }
        
    }
}

void MainHero::spawnObject(std::string objName, sf::Vector2f pos) {
     std::shared_ptr<StaticObject> newObj = std::make_shared<StaticObject>();
     newObj->initObject(Settings::getInstance()->getAssetPathByObjName(objName), pos);
     newObj->setName(objName);
     hudObjects->push_back(newObj);
}
