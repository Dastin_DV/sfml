#pragma once
#include <unordered_map>
#include "Enums.h"

class Settings {

public:

	Settings() {};

	std::string getAssetPathByObjName(std::string objName);
	std::string getSoundByLVL(LVL lvl);
	std::string getMapPathByLVL(LVL lvl);
	std::string getSoundByName(std::string name);
	std::string getHUDByGameState(GameState state);
	std::string getPathToFont();

	void setGameState(GameState newState);
	GameState getGameState();

	static Settings* getInstance();

	Settings(Settings & other) = delete;
	void operator=(const Settings&) = delete;

private:

	static Settings* instance;

	GameState currentState;

	std::unordered_map<std::string, std::string> nameToAsset {
		{"Coin",     "../Assets/Objects/coin1.png"},
		{"MainHero", "../Assets/Objects/hero1.png"},
		{"Sword",	 "../Assets/Objects/sword.png"},
		{"Enemy",	 "../Assets/Objects/poor-kid1.png"},
		{"EnemyDead","../Assets/Objects/poor-kid1-killed.png"},
		{"FastEnemy","../Assets/Objects/sage.png"},
		{"FastEnemyDead", "../Assets/Objects/sage-killed.png"},
		{"Chest",	 "../Assets/Objects/chest-closed.png"},
		{"Chest-opened", "../Assets/Objects/chest-opened.png"},
		{"TextHUD", "../Assets/HUD/HUD-div.png"}
	};

	std::unordered_map<LVL, std::string> LVLToSound {
		{LVL::LVL1, "../Assets/Sounds/music/determination.ogg"},
		{LVL::LVL2, "../Assets/Sounds/music/ghost-town.ogg"},
		{LVL::LVL3, "../Assets/Sounds/music/Sun Tribe.ogg"},
		{LVL::LVL4, "../Assets/Sounds/music/LastLVL.ogg"},
		{LVL::LVL5, "../Assets/Sounds/music/LastLVL.ogg"},
		{LVL::LVL6, "../Assets/Sounds/music/ghost-town.ogg"}
	};

	std::unordered_map<std::string, std::string> objNameToSound{
		{"Coin",     "../Assets/Sounds/fx/select.wav"},
		{"Exit",     "../Assets/Sounds/fx/lvlDone.ogg"},
		{"Sword",	 "../Assets/Sounds/fx/thorn.wav"},
		{"Enemy",    "../Assets/Sounds/fx/kill.ogg"},
		{"FastEnemy","../Assets/Sounds/fx/kill.ogg"},
		{"Chest",	 "../Assets/Sounds/fx/enter.wav"},
		{"MainMenu", "../Assets/Sounds/music/MainMenuGTA.ogg"}
	};

	std::unordered_map<LVL, std::string> LVLToMap {
		{LVL::LVL1, "../Assets/Lvls/LVL1.tmx"},
		{LVL::LVL2, "../Assets/Lvls/LVL2.tmx"},
		{LVL::LVL3, "../Assets/Lvls/LVL3.tmx"},
		{LVL::LVL4, "../Assets/Lvls/LVL4.tmx"},
		{LVL::LVL5, "../Assets/Lvls/LVL5.tmx"},
		{LVL::LVL6, "../Assets/Lvls/LVL6.tmx"}
	};

	std::unordered_map<GameState, std::string> gameStateToBackPicture{
		{GameState::GameOver, "../Assets/HUD/hud-tileset.png"},
		{GameState::Menu, "../Assets/HUD/Knight1.png"},
		{GameState::LevelSelection, "../Assets/HUD/ChoseLevel.png"},
		{GameState::Victory, "../Assets/HUD/Victory.png"}
	};

	std::string pathToFont = "../Assets/Fonts/Samson.ttf";
};