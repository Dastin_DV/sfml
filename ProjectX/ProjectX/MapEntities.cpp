#include <exception>
#include <math.h>
#include <algorithm>
#include <iostream>
#include <SFML/Audio.hpp>

#include "MapEntities.h"
#include "Exceptions.h"
#include "Settings.h"
#include "Helper.h"
#include "MainHero.h"
#include "Enemy.h"

void TileMapLVL::loadBackGroundSound(const std::string& path) {
	if (!backGroundBuffer.loadFromFile(path))
		throw std::runtime_error("Sound not found");

	backGroundSound.setBuffer(backGroundBuffer);
	backGroundSound.setLoop(true);
}

void TileMapLVL::playMusic() {
	backGroundSound.play();
	backGroundSound.setVolume(60);
}

bool TileMapLVL::loadLVLfromFile(const std::string fileName) {

	staticObjects = std::make_shared<std::vector<std::shared_ptr<StaticObject>>>();
	hudObjects = std::make_shared<std::vector<std::shared_ptr<StaticObject>>>();
	loadFont();

	//std::shared_ptr<StaticObject> textHUD = std::make_shared<StaticObject>();
	//textHUD->initObject(Settings::getInstance()->getAssetPathByObjName("TextHUD"));
	//textHUD->setName("TextHUD");
	//staticObjects->push_back(textHUD);

	tinyxml2::XMLDocument currentLVL;	//��������� ���� 

	auto status = currentLVL.LoadFile(fileName.c_str());
	if (status != tinyxml2::XMLError::XML_SUCCESS)
		throw ParsingException(status);

	auto root = currentLVL.FirstChildElement("map");

	mapWidth = root->FindAttribute("width")->IntValue();
	mapHeight = root->FindAttribute("height")->IntValue();
	tileWidth = root->FindAttribute("tilewidth")->IntValue();
	tileHeight =root->FindAttribute("tileheight")->IntValue();

	auto tileSet = root->FirstChildElement("tileset");
	firstTileId = tileSet->FindAttribute("firstgid")->IntValue();

	auto image = tileSet->FirstChildElement("image");

	tilesetImgPath = image->FindAttribute("source")->Value();

	loadImgAndCreateTexture(tilesetImgPath, tilesetTexture);

	tilesSetColumns = tilesetTexture.getSize().x / tileWidth;
	tileSetRows	= tilesetTexture.getSize().y / tileHeight;

	parseLayers(root);
	parseObjects(root);
	spawnObjects();
}

void TileMapLVL::parseLayers(tinyxml2::XMLElement* map) {

	auto currentLayer = map->FirstChildElement("layer");

	if (!currentLayer)
		throw std::runtime_error("No layers in the map, nothing to parse!");

	while (currentLayer) {												// ����������� �� �����
		parseLayer(currentLayer);
		currentLayer = currentLayer->NextSiblingElement("layer");		// ��������� ����
	}
}

void TileMapLVL::parseLayer(tinyxml2::XMLElement* currentLayer) {

	TileMapLayer newLayer;

	newLayer.name = currentLayer->FindAttribute("name")->Value();
	auto data = currentLayer->FirstChildElement("data");

	if (!data)
		throw std::runtime_error("No data tag in the map!");

	auto currentTile = data->FirstChildElement("tile");

	if (!currentTile)
		throw std::runtime_error("No one tile tag in the map!");

	int xWorldPos = 0;
	int yWorldPos = 0;

	leftUpCornerPos.x = xWorldPos;
	leftUpCornerPos.y = yWorldPos;

	if (newLayer.name == "Map")
		mapGrid = std::make_shared<std::vector<std::vector<int>>>(mapWidth, std::vector<int>(mapHeight));

	while (currentTile) {
		int actualTileId = 0;
		if (currentTile->FindAttribute("gid")) {
			int tileId = currentTile->FindAttribute("gid")->IntValue();

			determineGlobalID(tileId);
			actualTileId = tileId - firstTileId;

			sf::Sprite tileSprite;
			tileSprite.setTexture(tilesetTexture);

			int tileRowPosInTexture = actualTileId / tilesSetColumns;
			int tileColumnPosInTexture = actualTileId % tilesSetColumns;
			tileSprite.setTextureRect(sf::IntRect(tileColumnPosInTexture * tileHeight, tileRowPosInTexture * tileWidth, tileHeight, tileWidth));							// �������� ������ ����� ��������
			tileSprite.setPosition(xWorldPos * tileWidth, yWorldPos * tileHeight);

			Tile newTile;
			newTile.gid = actualTileId;
			newTile.sprite = tileSprite;

			newLayer.tiles.push_back(newTile);
		}
		//xWorldPos >= mapWidth ? xWorldPos = 0, yWorldPos++, (yWorldPos >= mapHeight ? yWorldPos = 0 : yWorldPos) : xWorldPos++;
		//auto vect = newLayer.grid->[0][0];
		if (newLayer.name == "Map")
			(*mapGrid)[xWorldPos][yWorldPos] = actualTileId;

		xWorldPos++;
		if (xWorldPos >= mapWidth)
		{
			xWorldPos = 0;
			yWorldPos++;
			if (yWorldPos >= mapHeight)
				yWorldPos = 0;
		}
		currentTile = currentTile->NextSiblingElement("tile");
	}

	layers.push_back(newLayer);
}

// ��� ���������� id, ����� ���������� ����� ��������.
void TileMapLVL::determineGlobalID(int& id) {

	if (id & FLIPPED_HORIZONTALLY_FLAG) {
		id = (id << 8) >> 8;
	}
	if (id & FLIPPED_VERTICALLY_FLAG) {
		id = (id << 16) >> 16;
	}
	if (id & FLIPPED_DIAGONALLY_FLAG) {
		id = (id << 24) >> 24;
	}
};

void TileMapLVL::parseObjects(tinyxml2::XMLElement* map) {
	auto currentObjectGroup = map->FirstChildElement("objectgroup");

	if (!currentObjectGroup)
		throw std::runtime_error("No object groups in the map!");

	while (currentObjectGroup) {														// ����������� �� ������� ��������
		parseObject(currentObjectGroup);
		currentObjectGroup = currentObjectGroup->NextSiblingElement("objectgroup");		// ��������� ������
	}
}

void TileMapLVL::parseObject(tinyxml2::XMLElement* currentObjectGroup) {
	auto currentObject = currentObjectGroup->FirstChildElement("object");

	if (!currentObject)
		throw std::runtime_error("No objects in the map!");

	while (currentObject) {													
		std::shared_ptr<TileMapObject> newObject;
		std::string name = currentObject->FindAttribute("name")->Value();

		if (name == "MainHero") {
			newObject = std::make_shared<MainHero>();
			mainHero = std::dynamic_pointer_cast<MainHero>(newObject);
		}
		else if (name == "Enemy") {
			newObject = std::make_shared<Enemy>();
		}
		else if (name == "FastEnemy") {
			newObject = std::make_shared<FastEnemy>();
		}
		else if (name == "Coin") {
			coinsInLVL++;
			newObject = std::make_shared<TileMapObject>();
		}
		else {
			name == "Sword" ? swordsInLVL++ : swordsInLVL;
			newObject = std::make_shared<TileMapObject>();
		}

		newObject->setMapGrid(mapGrid);
		newObject->id = currentObject->FindAttribute("id")->IntValue();
		newObject->name = currentObject->FindAttribute("name")->Value();
		newObject->type = currentObject->FindAttribute("type")->Value();
		newObject->xPos = currentObject->FindAttribute("x")->DoubleValue();
		newObject->yPos = currentObject->FindAttribute("y")->DoubleValue();
		objects.push_back(newObject);
		currentObject = currentObject->NextSiblingElement();		
	}
}

void TileMapLVL::spawnObjects() {
	sf::Sprite sprite;

	for (auto& object : objects) {
		object->loadTexture();
		sprite.setPosition(object->xPos, object->yPos);
		sprite.setTexture(object->texture);
		object->sprite = sprite;
	}
}

void TileMapLVL::updateObjects(sf::RenderWindow& window, sf::Event& event) {
	window.setKeyRepeatEnabled(false);
	std::unordered_set<int> idForDelete;
	sf::Vector2f prevMainHeroPos;
	sf::Vector2f currentMainHeroPos;

	bool wasStepMade = false;
	for (auto& object : objects) {
		if (object->name == "MainHero") {
			std::shared_ptr<MainHero> mainHero = std::dynamic_pointer_cast<MainHero>(object);

			prevMainHeroPos = mainHero->getSprite().getPosition(); 
			mainHero->setStaticObjects(staticObjects);
			mainHero->setHudObjects(hudObjects);
			mainHero->update(event);

			currentMainHeroPos = mainHero->getSprite().getPosition();

			wasStepMade = mainHero->wasStepMade();
			for (auto& otherObject : objects) {
				mainHero->checkCollisionWithObject(otherObject);
				if (!otherObject->isAlive)
					idForDelete.insert(otherObject->id);
			}
			if (mainHero->wasEnemyHit()) {
				mainHero->reduceSwordCount();		// ����������� ���������� ������ ����� �����. ����� ��� ���� ������� � ������, �� ����� ������� �� ����
				mainHero->clearWasEnemyHit();
			}
		}
		else if (object->name == "Enemy") {
			if (wasStepMade && !idForDelete.count(object->id)) {
				std::shared_ptr<Enemy> enemy = std::dynamic_pointer_cast<Enemy>(object);
				!enemy->isActiveEnemy() ? enemy->setMainHeroPos(currentMainHeroPos) : enemy->setMainHeroPos(prevMainHeroPos);
				enemy->update(event);
			}
		}
		else if (object->name == "FastEnemy") {
			if (wasStepMade && !idForDelete.count(object->id)) {
				std::shared_ptr<FastEnemy> fastEnemy = std::dynamic_pointer_cast<FastEnemy>(object);
				fastEnemy->setMainHeroPos(currentMainHeroPos);
				fastEnemy->setPrevMainHeroPos(prevMainHeroPos);
				fastEnemy->setObjects(objects);
				fastEnemy->setStaticObjects(staticObjects);
				fastEnemy->update(event);
			}
		}
		else
			object->update(event);
	}

	for (auto id : idForDelete)
		removeObjectById(id);
}

void TileMapLVL::updateObjectsWithDeltaTime(sf::Time deltaTime, sf::RenderWindow& window, sf::Event& event) {

	auto speed = 70.f;
	sf::Vector2f unit;
	float length;

	for (auto& hudObject : *hudObjects) {
		int nextIterationPointX, nextIterationPointY;
		if (hudObject->getName() == "Coin") {
			length = sqrt(pow(coinHUDpos.x - hudObject->getSprite().getPosition().x, 2) + pow(coinHUDpos.y - hudObject->getSprite().getPosition().y, 2));
			unit.x = (coinHUDpos.x - hudObject->getSprite().getPosition().x) / length;
			unit.y = (coinHUDpos.y - hudObject->getSprite().getPosition().y) / length;
			//nextIterationPointX = coinHUDpos.x - hudObject->getSprite().getPosition().x;
			//nextIterationPointY = coinHUDpos.y - hudObject->getSprite().getPosition().y;
		}
		else if (hudObject->getName() == "Sword") {
			length = sqrt(pow(swordHUDpos.x - hudObject->getSprite().getPosition().x, 2) + pow(swordHUDpos.y - hudObject->getSprite().getPosition().y, 2));
			unit.x = (swordHUDpos.x - hudObject->getSprite().getPosition().x) / length;
			unit.y = (swordHUDpos.y - hudObject->getSprite().getPosition().y) / length;
		}
		
		hudObject->getSprite().setPosition(hudObject->getSprite().getPosition().x + unit.x * deltaTime.asSeconds() * speed, hudObject->getSprite().getPosition().y + unit.y * deltaTime.asSeconds() * speed);
		std::cout << unit.x << " " << unit.y << std::endl;
		std::cout << hudObject->getSprite().getPosition().y << " " << coinHUDpos.y << std::endl;
		
		if ((int)hudObject->getSprite().getPosition().y == (int)coinHUDpos.y ||
			(int)hudObject->getSprite().getPosition().y == (int)swordHUDpos.y) {
			hudObject->setIsAlive(false);
		}
	}

	hudObjects->erase(std::remove_if(hudObjects->begin(), hudObjects->end(), [](std::shared_ptr<StaticObject> nextObj) {
		return !nextObj->isAlive();
	}), hudObjects->end());
}

void TileMapLVL::removeObjectById(int id) {
	objects.erase(std::remove_if(objects.begin(), objects.end(), [id](std::shared_ptr<TileMapObject> nextObj) {
		return nextObj->id == id;
	}), objects.end());
}

void TileMapLVL::draw(sf::RenderWindow& window) {
	for (int layer = 0; layer < layers.size(); layer++)
		for (int tile = 0; tile < layers[layer].tiles.size(); tile++)
			window.draw(layers[layer].tiles[tile].sprite);

	for (auto& object : *staticObjects) {
		window.draw(object->getSprite());
	}

	for (auto& object : objects) {
		object->draw(window);
	}

	for (auto& hudObject : *hudObjects) {
		window.draw(hudObject->getSprite());
	}
}

bool TileMapLVL::isLVLPassed(int heroCoins, bool chestFound, bool exitWasFound) {
	if (heroCoins == coinsInLVL && chestFound && exitWasFound) {
		return true;
	}
	return false;
}

void TileMapLVL::loadFont() {
	//loadFontAndCreateText(Settings::getInstance()->getPathToFont(), font, textCoins, 14);
	//loadFontAndCreateText(Settings::getInstance()->getPathToFont(), font, textSwords, 14);
}
