#pragma once
#include "SFML/Graphics.hpp"

void loadImgAndCreateTexture(std::string& pathToImg, sf::Texture& texture);
void loadFontAndCreateText(std::string pathToFont, sf::Font& font, sf::Text& text, int size);
bool absoluteToleranceCompare(double x, double y);