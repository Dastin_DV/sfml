#pragma once
#include <memory>

#include "Enums.h"
#include "MapEntities.h"
#include "Settings.h"
#include "GameOverScreen.h"
#include "MainMenuScreen.h"
#include "LevelSelectionScreen.h"
#include "VictoryScreen.h"

class GameWorld {

public:
	GameWorld(sf::RenderWindow& window) : window(window) {
		if (!completeBuffer.loadFromFile(Settings::getInstance()->getSoundByName("Exit")))
			throw std::runtime_error("Sound not found");

		completeSound.setBuffer(completeBuffer);
		completeSound.setLoop(false);
		currentMap = std::make_shared<TileMapLVL>();
		mapView = std::make_shared<sf::View>();
		miniMapView = std::make_shared<sf::View>();

		resizeView();
		window.setView(*mapView);
		// Screens
		gameOverScreen = std::make_shared<GameOverScreen>(window);
		mainMenuScreen = std::make_shared<MainMenuScreen>(window);
		levelSelectionScreen = std::make_shared<LevelSelectionScreen>(window, lvls.begin(), lvls.size());
		victoryScreen = std::make_shared<VictoryScreen>(window);
		// HUD
		initHUD();
		LVLit = lvls.begin();
	}

	void setNextLVL();
	void update(sf::RenderWindow& window, sf::Event& event);
	void update(sf::Time deltaTime, sf::RenderWindow& window, sf::Event& event);
	void draw(sf::RenderWindow& window);
	void initMap();
	void setLVLIt(std::vector<LVL>::iterator it) { this->LVLit = it; };
	std::vector<LVL>& getLvls() { return lvls; };
private:

	void initViewPort();
	void resizeView();
	void updateMapView();
	void initHUD();
	void updateHUD(std::shared_ptr<MainHero> mainHero);
	void drawHUD(sf::RenderWindow& window);

	LVL currentLVL;
	std::shared_ptr<TileMapLVL> currentMap;
	std::vector<LVL> lvls { LVL::LVL1, LVL::LVL2, LVL::LVL3, LVL::LVL4, LVL::LVL5, LVL::LVL6 };
	std::vector<LVL>::iterator LVLit;

	sf::SoundBuffer completeBuffer;
	sf::Sound completeSound;

	sf::Font font;
	sf::Texture coinTexture;
	sf::Texture swordTexture;
	sf::Texture backHUDTexture;
	sf::Sprite coinHUD;
	sf::Sprite swordHUD;
	sf::Sprite backHUD;
	sf::Text swordText;
	sf::Text coinsText;

	std::shared_ptr<sf::View> mapView;
	const float mapViewHeight = 150.f;

	sf::RectangleShape miniMapBorder;
	std::shared_ptr<sf::View> miniMapView;
	float miniMapHeight = 500.f;

	sf::RenderWindow& window;

	std::shared_ptr<GameOverScreen> gameOverScreen;
	std::shared_ptr<MainMenuScreen> mainMenuScreen;
	std::shared_ptr<LevelSelectionScreen> levelSelectionScreen;
	std::shared_ptr<VictoryScreen> victoryScreen;
};