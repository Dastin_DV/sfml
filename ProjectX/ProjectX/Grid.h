#pragma once
#include "SFML/Graphics.hpp"

struct GridObject {
	float width;
	float height;
	sf::Vector2f pos;
};

class Grid {
public:

	Grid(float col, float row) : col(col), row(row) {
		Init();
	}

	Grid() {		// ���� ���� ������������� ������� Init �������.

	}

	void Init() {
		std::vector<std::vector<GridObject>> initialGrid(row, std::vector<GridObject>(col));
		gridObjects = std::move(initialGrid);
	}

	void setPosition(sf::Vector2f pos) { this->pos = pos; }
	void setWorldSize(sf::Vector2f size) { this->worldSize = size; }
	void createGrid();
	void setOffsetX(float x) { this->offsetX = x; }
	void setOffsetY(float y) { this->offsetY = y; }
	void setRows(int rows) { this->row = rows; }
	void setColumns(int columns) { this->col = columns; }
	void setScale(float scaleX, float scaleY) { this->offsetScaleX = scaleX; this->offsetScaleY = scaleY;};
	void resize(float scaleX, float scaleY);

	std::vector<std::vector<GridObject>>& getGridObjects() { return this->gridObjects; };
	sf::Vector2f getObjectSize() { return objectSize; };
	sf::Vector2f getWorldSize() { return worldSize; };
	sf::Vector2f getPosition() { return pos; }

private:

	void clearGrid();

	float col;
	float row;
	float offsetX = 0;
	float offsetY = 0;
	float offsetScaleX = 1.f;
	float offsetScaleY = 1.f;

	sf::Vector2f origin;
	sf::Vector2f pos;
	sf::Vector2f worldSize;

	sf::Vector2f objectSize;
	std::vector<std::vector<GridObject>> gridObjects;
};