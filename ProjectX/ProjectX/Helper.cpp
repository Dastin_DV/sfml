#include "Helper.h"

void loadImgAndCreateTexture(std::string& pathToImg, sf::Texture& texture) {
	if (!pathToImg.size()) return;

	sf::Image img;
	if (!img.loadFromFile(pathToImg))
		throw std::runtime_error("Img file couldn't be opened or found! Path: " + pathToImg);

	img.createMaskFromColor(sf::Color(255, 255, 255));
	texture.loadFromImage(img);
	texture.setSmooth(false);
}

void loadFontAndCreateText(std::string pathToFont, sf::Font& font, sf::Text& text, int size) {
	if (!pathToFont.size()) return;

	if (!font.loadFromFile(pathToFont))
		throw std::runtime_error("Font file couldn't be opened or found! Path: " + pathToFont);

	text.setFont(font);
	text.setCharacterSize(size);
}

bool absoluteToleranceCompare(double x, double y)
{
	return std::fabs(x - y) <= std::numeric_limits<double>::epsilon() * 15.f;
}