#include "VictoryScreen.h"

void VictoryScreen::init(sf::RenderWindow& window) {
	auto view = window.getView();
	auto targetSize = view.getSize();
	float scaleX = targetSize.x / spriteBack.getLocalBounds().width;
	float scaleY = targetSize.y / spriteBack.getLocalBounds().height;

	view.setCenter(0 + spriteBack.getLocalBounds().width / 2 * scaleX,
		0 + spriteBack.getLocalBounds().height / 2 * scaleY);

	spriteBack.setScale(scaleX, scaleY);

	window.setView(view);
	initExitButton(window);
	initSelectLVLButton(window);
	//clearButtonsStates();
}

void VictoryScreen::update(sf::RenderWindow& window, sf::Event& event) {
	choseLVLButton.update(event, window);
	if (choseLVLButton.getState() == myGui::state::clicked) {
		Settings::getInstance()->setGameState(GameState::LevelSelection);
	}

	exitButton.update(event, window);
	if (exitButton.getState() == myGui::state::clicked) {
		Settings::getInstance()->setGameState(GameState::Exit);
	}
}

void VictoryScreen::draw(sf::RenderWindow& window) {
	text.setPosition(window.getView().getCenter().x - text.getLocalBounds().width / 2, window.getView().getCenter().y - window.getView().getSize().y / 2);
	window.draw(spriteBack);
	window.draw(choseLVLButton);
	window.draw(exitButton);
	window.draw(text);
}

void VictoryScreen::initSelectLVLButton(sf::RenderWindow& window) {
	myGui::button button;
	button.setText("Chose LVL");
	button.setFont(font);
	button.setStyle(myGui::style::save);
	button.setBorderRadius(1.f);
	button.setBorderThickness(1.f);
	button.setSize(12);
	button.setPosition({ window.getView().getCenter().x, window.getView().getCenter().y + ButtonYOffset });

	choseLVLButton = button;
}

void VictoryScreen::initExitButton(sf::RenderWindow& window) {
	myGui::button button;
	button.setText("Exit");
	button.setFont(font);
	button.setStyle(myGui::style::save);
	button.setBorderRadius(1.f);
	button.setBorderThickness(1.f);
	button.setSize(choseLVLButton.getDimensions(), 12);
	button.setPosition({ window.getView().getCenter().x, window.getView().getCenter().y + 2 * ButtonYOffset });

	//std::cout << "ExitButton position x: " << button.getPosition().x << " y: " << button.getPosition().y << std::endl;
	exitButton = std::move(button);
}