#include "LevelSelectionScreen.h"

void LevelSelectionScreen::update(sf::RenderWindow& window, sf::Event& event) {
	if (event.type == sf::Event::Resized) {
		resize(window);
	}

	int i = 0;
	for (auto& button : lvlButtons) {
		button.update(event, window);
		if (i < lvlCount && button.getState() == myGui::state::clicked) {
			Settings::getInstance()->setGameState(GameState::Game);
			this->LVLit = next(LVLit, i);
		}
		i++;

		if (button.getButtonName() == "Exit" && button.getState() == myGui::state::clicked) {
			Settings::getInstance()->setGameState(GameState::Exit);
		}

		if (button.getButtonName() == "MainMenu" && button.getState() == myGui::state::clicked) {
			Settings::getInstance()->setGameState(GameState::Menu);
		}
	}
}

void LevelSelectionScreen::draw(sf::RenderWindow& window) {
	window.draw(spriteBack);
	//lvlButtons.clear();
	for (auto& button : lvlButtons) {
		window.draw(button);
		std::cout << button.getPosition().x << " " << button.getPosition().y << std::endl;
		//std::cout << window.getView().getCenter().x << " " << window.getView().getCenter().y << std::endl;
	}
}

void LevelSelectionScreen::resize(sf::RenderWindow& window) {
	float aspectRatio = float(window.getSize().x) / float(window.getSize().y);
	//auto viewRef = window.getView();
	//viewRef.setSize(screenHeight * aspectRatio, screenHeight);
	//std::cout << "viewRef size: " << viewRef.getSize().x << " " << viewRef.getSize().y << std::endl;
	//window.setView(viewRef);
	initView(window);
	//viewRef.setSize(screenHeight * aspectRatio, screenHeight);
	//window.setView(viewRef);

	lvlButtons.clear();
	buttonsWrapper->Init();
	buttonsWrapper->setWorldSize({ window.getView().getSize().x, window.getView().getSize().y / 4.f });
	std::cout << "ButtonsWrapper size: " << buttonsWrapper->getWorldSize().x << " " << buttonsWrapper->getWorldSize().y << std::endl;
	buttonsWrapper->setPosition({ window.getView().getCenter().x - buttonsWrapper->getWorldSize().x / 2, window.getView().getCenter().y });
	buttonsWrapper->resize(scaleX, scaleY);
	initButtons(buttonsWrapper->getGridObjects());

	anotherButtonWrapper->Init();
	anotherButtonWrapper->setWorldSize({ window.getView().getSize().x, window.getView().getSize().y / 4.f });
	std::cout << "AnotherButtonWrapper size: " << anotherButtonWrapper->getWorldSize().x << " " << anotherButtonWrapper->getWorldSize().y << std::endl;
	anotherButtonWrapper->setPosition({ (window.getView().getCenter().x - buttonsWrapper->getWorldSize().x / 2), ((buttonsWrapper->getPosition().y + buttonsWrapper->getWorldSize().y * 1.5f)) });
	anotherButtonWrapper->resize(scaleX, scaleY);
	initSettingButtons(anotherButtonWrapper->getGridObjects());
}

void LevelSelectionScreen::init(sf::RenderWindow& window) {

	screenHeight = window.getView().getSize().y;
	initView(window);
	std::cout << "ScreenHeight " << screenHeight << std::endl;

	buttonsWrapper = std::make_shared<Grid>();
	buttonsWrapper->setColumns(3);
	buttonsWrapper->setRows(3);
	buttonsWrapper->Init();

	anotherButtonWrapper = std::make_shared<Grid>();
	anotherButtonWrapper->setColumns(2);
	anotherButtonWrapper->setRows(1);
	anotherButtonWrapper->Init();

	resize(window);
}

void LevelSelectionScreen::initView(sf::RenderWindow& window) {

	/*float aspectRatio = float(window.getSize().x) / float(window.getSize().y);
	auto viewRef = window.getView();
	viewRef.setSize(screenHeight * aspectRatio, screenHeight);
	std::cout << "viewRef size: " << viewRef.getSize().x << " " << viewRef.getSize().y << std::endl;
	window.setView(viewRef);*/

	auto view = window.getView();
	auto targetSize = view.getSize();
	scaleX = targetSize.x / spriteBack.getLocalBounds().width;
	scaleY = targetSize.y / spriteBack.getLocalBounds().height;
	std::cout << "scaleX " << scaleX << " " << "scaleY " << scaleY << std::endl;
	//view.setSize(viewHeight * aspectRatio, viewHeight);

	view.setCenter(0 + spriteBack.getLocalBounds().width / 2 * scaleX,
		0 + spriteBack.getLocalBounds().height / 2 * scaleY);

	std::cout << "ViewCenter " << view.getCenter().x << " " << " " << view.getCenter().y << std::endl;
	//std::cout << "Initial Center " << window.getView().getCenter().x << " " << window.getView().getCenter().y << std::endl;
	spriteBack.setScale(scaleX, scaleY);
	spriteBack.setPosition(0,0);
	window.setView(view);
}

void LevelSelectionScreen::initButtons(std::vector<std::vector<GridObject>>& gridObjects) {
	lvlButtons.clear();
	int count = 1;
	for (auto& row : gridObjects) {
		for (auto& object : row) {
			createButton(object, "LVL " + std::to_string(count));
			std::cout << "Button size: " << object.width << " " << object.height << std::endl;
			std::cout << "Button position: " << object.pos.x << " " << object.pos.y << std::endl;
			count++;
		}
	}
}

void LevelSelectionScreen::initSettingButtons(std::vector<std::vector<GridObject>>& gridObjects) {
	std::vector<std::string> buttonName{ "MainMenu", "Exit" };
	int i = 0;
	for (auto& row : gridObjects) {
		for (auto& object : row) {
			createButton(object, buttonName[i++]);
		}
	}
}

void LevelSelectionScreen::createButton(GridObject& object, std::string buttonName) {
	if (object.height != 0 && object.width != 0) {
		myGui::button button;
		button.setText(buttonName);
		button.setButtonName(buttonName);
		button.setFont(font);
		button.setStyle(myGui::style::none);
		button.setBorderRadius(1.f);
		button.setBorderThickness(1.f);
		button.setColorNormal(sf::Color(200, 0, 200, 255));
		button.setColorHover(sf::Color(255, 0, 255, 100));
		button.setColorClicked(sf::Color(150, 0, 150, 255));
		button.setColorTextNormal(sf::Color(255, 255, 255, 255));
		button.setColorTextHover(sf::Color(255, 255, 0, 255));
		button.setColorTextClicked(sf::Color(255, 0, 0, 255));
		//std::cout << "Button1 size: " << object.width << " " << object.height << std::endl;
		//std::cout << "Button1 position: " << object.pos.x << " " << object.pos.y << std::endl;
		button.setSize({ object.width, object.height}, 10);
		button.setPosition({ object.pos.x, object.pos.y });
		lvlButtons.push_back(button);
	}
}