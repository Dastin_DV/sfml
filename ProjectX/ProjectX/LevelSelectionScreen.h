#pragma once
#include "SFML/Graphics.hpp"
#include "Helper.h"
#include "Settings.h"
#include "Button.h"
#include "Grid.h"

class LevelSelectionScreen {

public:
	LevelSelectionScreen(sf::RenderWindow& window, std::vector<LVL>::iterator begin, int lvlCount = 0) {
		auto pathToHUD = Settings::getInstance()->getHUDByGameState(GameState::LevelSelection);
		auto pathToFont = Settings::getInstance()->getPathToFont();
		loadImgAndCreateTexture(pathToHUD, textureBack);
		loadFontAndCreateText(pathToFont, font, text, 16);
		spriteBack.setTexture(textureBack);
		init(window);
		this->LVLit = begin;
		this->lvlCount = lvlCount;
	}

	void update(sf::RenderWindow& window, sf::Event& event);
	void draw(sf::RenderWindow& window);
	void init(sf::RenderWindow& window);
	void initView(sf::RenderWindow& window);
	void initButtons(std::vector<std::vector<GridObject>>& gridObjects);
	void initSettingButtons(std::vector<std::vector<GridObject>>& gridObjects);
	void createButton(GridObject& object, std::string buttonName);
	void resize(sf::RenderWindow& window);

	void setLVLIterator(std::vector<LVL>::iterator it) { this->LVLit = it; };
	std::vector<LVL>::iterator getLVLIterator() { return LVLit; };

private:
	sf::Texture textureBack;
	sf::Sprite spriteBack;
	sf::Font font;
	sf::Text text;

	float scaleX;
	float scaleY;
	std::vector<myGui::button> lvlButtons;
	std::shared_ptr<Grid> buttonsWrapper;
	std::shared_ptr<Grid> anotherButtonWrapper;

	std::vector<LVL>::iterator LVLit;
	int lvlCount = 0;

	float screenHeight;
};