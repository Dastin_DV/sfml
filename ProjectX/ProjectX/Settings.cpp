#include "Settings.h"

Settings* Settings::getInstance() {
    if (!instance)
        instance = new Settings();

    return instance;
}

void Settings::setGameState(GameState newState) {
    this->currentState = newState;
}

GameState Settings::getGameState() {
    return this->currentState;
}

std::string Settings::getAssetPathByObjName(std::string objName) {
    if (nameToAsset.count(objName)) {
        return nameToAsset.at(objName);
    } else return "";
}

std::string Settings::getSoundByLVL(LVL lvl) {
    if (LVLToSound.count(lvl)) {
        return LVLToSound.at(lvl);
    }
    else return "";
}

std::string Settings::getMapPathByLVL(LVL lvl) {
    if (LVLToMap.count(lvl)) {
        return LVLToMap.at(lvl);
    }
    else return "";
}

std::string Settings::getSoundByName(std::string name) {
    if (objNameToSound.count(name)) {
        return objNameToSound.at(name);
    }
    else return "";
}

std::string Settings::getHUDByGameState(GameState state) {
    if (gameStateToBackPicture.count(state)) {
        return gameStateToBackPicture.at(state);
    }
    else return "";
}

std::string Settings::getPathToFont() {
    return this->pathToFont;
}

Settings* Settings::instance = nullptr;