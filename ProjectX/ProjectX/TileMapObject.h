#pragma once
#include "SFML/Graphics.hpp"
#include "StaticObject.h"

#include <unordered_set>

struct TileMapObject {
	std::string name;
	std::string type;
	double xPos, yPos;
	int id;
	bool isAlive = true;

	sf::Rect<int> rect;
	sf::Texture texture;
	sf::Sprite sprite;
	std::shared_ptr<std::vector<std::vector<int>>> grid;
	const std::unordered_set<int> walkableId{ 26, 29, 92, 52 };

	void loadTexture();
	void draw(sf::RenderWindow& window);
	std::pair<int, int> getTilePosByWorldCoordinates(sf::Vector2f neigbourPos);
	bool isNeighbourTileWalkable(sf::Vector2f neigbourPos);
	bool isLineTileWalkable(sf::Vector2f from, sf::Vector2f to);
	void setMapGrid(std::shared_ptr<std::vector<std::vector<int>>> grid) { this->grid = grid; }
	sf::Sprite& getSprite() { return this->sprite; };

	virtual void update(sf::Event& event) {};
	virtual void checkCollisionWithObject(std::shared_ptr<TileMapObject>& object) {};
};