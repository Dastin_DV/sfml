#include <iostream>

#include "GameWorld.h"
#include "Settings.h"
#include "MainHero.h"

void GameWorld::setNextLVL() {
	currentMap->loadLVLfromFile(Settings::getInstance()->getMapPathByLVL(*LVLit)); 
	miniMapHeight = currentMap->getMapPixelHeight();
	std::cout << "MiniMap Height: " << miniMapHeight << std::endl;
	initViewPort();
	currentMap->loadBackGroundSound(Settings::getInstance()->getSoundByLVL(*LVLit));
	if (Settings::getInstance()->getGameState() == GameState::Game) {
		currentMap->playMusic();
	}
}

void GameWorld::initMap() {
	std::shared_ptr<TileMapLVL> newMap = std::make_shared<TileMapLVL>();
	currentMap = newMap;
	setNextLVL();
}

void GameWorld::update(sf::RenderWindow& window, sf::Event& event) {
	if (event.type == sf::Event::Resized) {
		resizeView();
	}

	if (Settings::getInstance()->getGameState() == GameState::Game) {
		currentMap->updateObjects(window, event);
		currentMap->setCoinHUDPos(coinHUD.getPosition());
		currentMap->setSwordHUDPos(swordHUD.getPosition());

		auto mainHero = currentMap->getMainHero();
		mapView->setCenter(mainHero->sprite.getPosition());
		updateMapView();
		updateHUD(mainHero);

		if (currentMap->isLVLPassed(mainHero->getCoinCount(), mainHero->chestWasFound(), mainHero->exitWasFound())) {
			if (LVLit == prev(lvls.end())) {
				window.setView(*mapView);
				victoryScreen->init(window);
				Settings::getInstance()->setGameState(GameState::Victory);
			}
			else {
				LVLit != prev(lvls.end()) ? LVLit++ : LVLit;
				initMap();
				std::cout << "LVL passed!" << std::endl;
				completeSound.play();
			}
		}
	}
	else if (Settings::getInstance()->getGameState() == GameState::Menu) {
		window.setView(*mapView);
		mainMenuScreen->init(window);
		mainMenuScreen->update(window, event);
		mainMenuScreen->playMusic();
		if (Settings::getInstance()->getGameState() == GameState::Game) {
			initMap();
		}
	}
	else if (Settings::getInstance()->getGameState() == GameState::Escape) {
		window.setView(*mapView);
		mainMenuScreen->init(window);
		mainMenuScreen->update(window, event);
		mainMenuScreen->playMusic();
	}
	else if (Settings::getInstance()->getGameState() == GameState::LevelSelection) {
		window.setView(*mapView);
		levelSelectionScreen->setLVLIterator(lvls.begin());
		levelSelectionScreen->initView(window);
		levelSelectionScreen->update(window, event);
		if (Settings::getInstance()->getGameState() == GameState::Game) {
			LVLit = levelSelectionScreen->getLVLIterator();
			initMap();
		}
	}
	else if (Settings::getInstance()->getGameState() == GameState::GameOver) {
		window.setView(*mapView);
		gameOverScreen->init(window);
		gameOverScreen->update(window, event);
		if (Settings::getInstance()->getGameState() == GameState::Game) {
			initMap();
		}
		std::cout << "Game over!" << std::endl;
	} 
	else if (Settings::getInstance()->getGameState() == GameState::Victory) {
		victoryScreen->update(window, event);
	}
	else if (Settings::getInstance()->getGameState() == GameState::Exit) {
		window.close();
	}
}

void GameWorld::update(sf::Time deltaTime, sf::RenderWindow& window, sf::Event& event) {
	if (Settings::getInstance()->getGameState() == GameState::Game) {
		currentMap->updateObjectsWithDeltaTime(deltaTime, window, event);
	}
}

void GameWorld::draw(sf::RenderWindow& window) {
	if (Settings::getInstance()->getGameState() == GameState::Game) {
		auto mainHero = currentMap->getMainHero();
		//mapView->setCenter(mainHero->getSprite().getPosition());
		window.setView(*mapView);
		currentMap->draw(window);
		drawHUD(window);
		window.setView(*miniMapView);
		currentMap->draw(window);
		window.draw(miniMapBorder);
	}
	else if (Settings::getInstance()->getGameState() == GameState::Menu || Settings::getInstance()->getGameState() == GameState::Escape) {
		mainMenuScreen->draw(window);
	}
	else if (Settings::getInstance()->getGameState() == GameState::LevelSelection) {
		levelSelectionScreen->draw(window);
	}
	else if (Settings::getInstance()->getGameState() == GameState::GameOver){
		gameOverScreen->draw(window);
	}
	else if (Settings::getInstance()->getGameState() == GameState::Victory) {
		victoryScreen->draw(window);
	}
}

void GameWorld::initViewPort() {
	auto playerPos = currentMap->getMainHero()->sprite.getPosition();
	float aspectRatio = float(window.getSize().x) / float(window.getSize().y);
	float miniMapBorderThickness = 10.f;

	mapView->setCenter(playerPos);
	std::cout << "Map view: " << mapView->getCenter().x << " " << mapView->getCenter().y << std::endl;
	std::cout << "Mini map view: " << miniMapView->getCenter().x << " " << miniMapView->getCenter().y << std::endl;
	resizeView();

	miniMapBorder.setSize(sf::Vector2f(currentMap->getMapPixelWidth() - 2 * miniMapBorderThickness, miniMapHeight - 2 * miniMapBorderThickness));
	miniMapBorder.setPosition(sf::Vector2f(0.7f * (miniMapView->getCenter().x - miniMapView->getSize().x / 2) + miniMapBorderThickness, 0.7f * (miniMapView->getCenter().y - miniMapView->getSize().y / 2) + miniMapBorderThickness));
	miniMapBorder.setFillColor(sf::Color::Transparent);
	miniMapBorder.setOutlineThickness(10.f);
	miniMapBorder.setOutlineColor(sf::Color::Red);

	miniMapView->setViewport(sf::FloatRect(0.7f, 0.65f, 0.3f, 0.3f));

	window.setView(*mapView);
}

void GameWorld::resizeView() {
	float aspectRatio = float(window.getSize().x) / float(window.getSize().y);
	mapView->setSize(mapViewHeight * aspectRatio, mapViewHeight);
	miniMapView->setSize(miniMapHeight * aspectRatio, miniMapHeight);
	miniMapView->setCenter(0 + miniMapView->getSize().x / 2, 0 + miniMapView->getSize().y / 2);
	miniMapView->zoom(1);
	//HUDView->setSize(HUDViewHeight * aspectRatio, HUDViewHeight);
}

void GameWorld::updateMapView() {
	float currentLeftViewBorder = mapView->getCenter().x - mapView->getSize().x / 2;
	float currentUpViewBorder = mapView->getCenter().y - mapView->getSize().y / 2;
	float currentDownViewBorder = mapView->getCenter().y + mapView->getSize().y / 2;
	float currentRightViewBorder = mapView->getCenter().x + mapView->getSize().x / 2;

		// Up
	if (currentUpViewBorder < currentMap->getLeftUpCornerPos().y) {
		mapView->setCenter(mapView->getCenter().x, currentMap->getLeftUpCornerPos().y + mapView->getSize().y / 2);
	}	// Left
	if (currentLeftViewBorder < currentMap->getLeftUpCornerPos().x) {
		mapView->setCenter(currentMap->getLeftUpCornerPos().x + mapView->getSize().x / 2, mapView->getCenter().y);
	}	// Down
	if (currentDownViewBorder > currentMap->getLeftUpCornerPos().y + (currentMap->getMapPixelHeight())) {
		mapView->setCenter(mapView->getCenter().x, currentMap->getLeftUpCornerPos().y + (currentMap->getMapPixelHeight()) - mapView->getSize().y / 2);
	}	// Right
	if (currentRightViewBorder > currentMap->getLeftUpCornerPos().x + (currentMap->getMapPixelHeight())) {
		mapView->setCenter(currentMap->getLeftUpCornerPos().x + (currentMap->getMapPixelHeight()) - mapView->getSize().x / 2, mapView->getCenter().y);
	}
}

void GameWorld::initHUD() {
	// HUD
	loadFontAndCreateText(Settings::getInstance()->getPathToFont(), font, coinsText, 14);
	loadFontAndCreateText(Settings::getInstance()->getPathToFont(), font, swordText, 14);
	auto pathToCoinTexture = Settings::getInstance()->getAssetPathByObjName("Coin");
	auto pathToSwordTexture = Settings::getInstance()->getAssetPathByObjName("Sword");
	auto pathToBackHUDTexture = Settings::getInstance()->getAssetPathByObjName("TextHUD");
	loadImgAndCreateTexture(pathToCoinTexture, coinTexture);
	coinHUD.setTexture(coinTexture);
	coinHUD.setScale({ 1.2f, 1.2f });
	loadImgAndCreateTexture(pathToSwordTexture, swordTexture);
	swordHUD.setTexture(swordTexture);
	swordHUD.setScale({ 1.2f, 1.2f });
	loadImgAndCreateTexture(pathToBackHUDTexture, backHUDTexture);
	backHUD.setTexture(backHUDTexture);
	backHUD.setScale({ 1.f, 1.25f });
}

void GameWorld::updateHUD(std::shared_ptr<MainHero> mainHero) {
	backHUD.setPosition(mapView->getCenter().x - mapView->getSize().x / 2, mapView->getCenter().y - mapView->getSize().y / 2);
	backHUD.setScale({ mapView->getSize().x / backHUD.getLocalBounds().width, 1.25f });
	coinHUD.setPosition(mapView->getCenter().x - mapView->getSize().x / 2, mapView->getCenter().y - mapView->getSize().y / 2);
	coinsText.setString(std::to_string(mainHero->getCoinCount()) + " - " + std::to_string(currentMap->getCoinsCountInLVL()));
	coinsText.setPosition(mapView->getCenter().x - mapView->getSize().x / 2 + coinHUD.getLocalBounds().width, mapView->getCenter().y - mapView->getSize().y / 2);
	swordText.setString(std::to_string(mainHero->getSwordCount()) + " - " + std::to_string(currentMap->getSwordsCountInLVL()));
	swordText.setPosition(mapView->getCenter().x + mapView->getSize().x / 2 - swordText.getLocalBounds().width, mapView->getCenter().y - mapView->getSize().y / 2);
	swordHUD.setPosition(swordText.getPosition().x - swordHUD.getLocalBounds().width, mapView->getCenter().y - mapView->getSize().y / 2);
}

void GameWorld::drawHUD(sf::RenderWindow& window) {
	window.draw(backHUD);
	window.draw(coinHUD);
	window.draw(coinsText);
	window.draw(swordHUD);
	window.draw(swordText);
}