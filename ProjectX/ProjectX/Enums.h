#pragma once

enum class ObjActions {
	To�ollect,
	Spawn,
	ToFight,
	End
};

enum class GameState {
	Menu,
	Escape,
	Game,
	GameOver,
	LevelSelection,
	Exit,
	Victory
};


enum class ObjType {
	Coin,
	Sword,
	MainHero,
	Enemy,
	Exit
};

enum class LVL {
	LVL1,
	LVL2,
	LVL3,
	LVL4,
	LVL5,
	LVL6
};