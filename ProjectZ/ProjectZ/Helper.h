#pragma once
#include <vector>
#include <algorithm>

template <typename T>
std::vector<std::vector<T>> splitIntoChunks(std::vector<T>& container, int chunksNum) {
	std::vector<std::vector<T>> chunks;

	auto beginIt = container.begin();
	auto endIt = beginIt;

	int shift = 0;
	int shiftStep = container.size() / chunksNum + container.size() % chunksNum;

	while (shift != container.size()) {
		shift = std::clamp(shift + shiftStep, 0, (int)container.size());
		beginIt = endIt;
		endIt = next(container.begin(), shift);
		std::vector<T> newChunk(beginIt, endIt);
		chunks.push_back(newChunk);
	}

	return chunks;
}