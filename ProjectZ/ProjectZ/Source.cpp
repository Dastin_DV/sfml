#include <thread>
#include <functional> 

#include <SFML/Graphics.hpp>
#include "PhysarumPolycephalum.h"

const sf::Time TimePerFrame = sf::seconds(1.f / 60.f);

int main()
{
    sf::RenderWindow window(sf::VideoMode(1000, 1000), "SFML works!");
    sf::CircleShape shape(100.f);
    shape.setFillColor(sf::Color::Green);
    Physarum physarum(10000);
    sf::Clock clock;
    sf::Time timeSinceLastUpdate = sf::Time::Zero;
    

    while (window.isOpen())
    {
        timeSinceLastUpdate += clock.restart();
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        while (timeSinceLastUpdate > TimePerFrame)
        {
            window.clear();
            timeSinceLastUpdate -= TimePerFrame;

            std::vector<std::thread> threads;
            //auto chunks = physarum.getChunks();
           /* for (int i = 0; i < chunks.size(); i++) {
                std::thread thr(&Physarum::updateInParallel, &physarum, std::ref(window), std::ref(chunks[i]), TimePerFrame);
                threads.push_back(std::move(thr));
            }

            for (auto& thr : threads) {
                thr.join();
            }*/

            physarum.update(window, TimePerFrame);
            //physarum.setChunks(chunks);
            physarum.draw(window);
            window.display();
        }
    }

    return 0;
}