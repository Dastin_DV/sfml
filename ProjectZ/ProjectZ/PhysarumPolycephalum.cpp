#include <math.h>
#include <algorithm>
#include <iostream>
#include <chrono>
#include <thread>
#include <execution>
#include <future>

#include "PhysarumPolycephalum.h"
#include "Helper.h"

#define PI 3.14159265
// Thank you Sebastian Lague
unsigned int hash(unsigned int state)
{
	state ^= 2747636419u;
	state *= 2654435769u;
	state ^= state >> 16;
	state *= 2654435769u;
	state ^= state >> 16;
	state *= 2654435769u;
	return state;
}

float scaleToRange01(unsigned int state)
{
	return state / 4294967295.0;
}

int GetRandomNumber(int min, int max)
{
	srand(time(NULL));
	int num = min + rand() % (max - min + 1);

	return num;
}

float degreesToRadians(float degrees) {
	return degrees * PI / 180;
}

Physarum::Physarum(const int numAgents) {
	agents = new std::vector<Agent>(numAgents);
	initAgents();
}

void Physarum::initAgents() {
	int id = 0;
	for (auto& item : *agents) {
		item.rec.setSize(sf::Vector2f(1.f, 1.f));
		item.position.x = 300;
		item.position.y = 300;
		item.rec.setPosition(item.position);
		item.angle = GetRandomNumber(0, 360);
		item.id = id++;
	}

	chunks = splitIntoChunks<Agent>(*agents, 8);
}

void Physarum::update(sf::RenderWindow& window, sf::Time deltaTime){

	std::vector<std::future<void>> futures;

	for (auto& chunk : chunks) {
		auto chunkFuture = std::async([&window, &chunk, &deltaTime] {
			auto viewport = window.getView().getViewport();
			sf::Vector2f border = sf::Vector2f(window.getView().getSize().x, window.getView().getSize().y);

			for (auto& agent : chunk) {
				auto now = std::chrono::system_clock::now().time_since_epoch();
				auto count = std::chrono::duration_cast<std::chrono::seconds>(now).count();

				unsigned int random = hash(agent.position.y * border.x + agent.position.x + hash(agent.id + (count + deltaTime.asMilliseconds()) * 100000));

				sf::Vector2f direction = sf::Vector2f(cosf(degreesToRadians(agent.angle)), sin(degreesToRadians(agent.angle)));
				sf::Vector2f newPos = agent.position + direction * 0.1f * (float)deltaTime.asMilliseconds();

				//std::cout << "NewAgentPos: " << newPos.x << " " << newPos.y << std::endl;

				if (newPos.x < 0 || newPos.x >= border.x || newPos.y < 0 || newPos.y >= border.y) {
					float randomAngle = scaleToRange01(random) * 2 * 180;

					newPos.x = std::fmin(border.x - 1, std::fmax(0, newPos.x));
					newPos.y = std::fmin(border.y - 1, std::fmax(0, newPos.y));
					agent.angle = randomAngle;
					//agent.rec.setFillColor(sf::Color(scaleToRange01(random) * 255, scaleToRange01(random) * 255, scaleToRange01(random) * 255));
					//std::cout << "AgentAngle: " << agent.angle << std::endl;
				}

				agent.position = newPos;
				agent.rec.setPosition(newPos);
			}
		});

		futures.push_back(std::move(chunkFuture));
	}

	for (auto& item : futures)
		item.get();


	//for (auto& chunk : chunks) {
	//	auto viewport = window.getView().getViewport();
	//	sf::Vector2f border = sf::Vector2f(window.getView().getSize().x, window.getView().getSize().y);

	//	for (auto& agent : chunk) {
	//		auto now = std::chrono::system_clock::now().time_since_epoch();
	//		auto count = std::chrono::duration_cast<std::chrono::seconds>(now).count();

	//		unsigned int random = hash(agent.position.y * border.x + agent.position.x + hash(agent.id + (count + deltaTime.asMilliseconds()) * 100000));

	//		sf::Vector2f direction = sf::Vector2f(cosf(degreesToRadians(agent.angle)), sin(degreesToRadians(agent.angle)));
	//		sf::Vector2f newPos = agent.position + direction * 0.3f * (float)deltaTime.asMilliseconds();

	//		//std::cout << "NewAgentPos: " << newPos.x << " " << newPos.y << std::endl;

	//		if (newPos.x < 0 || newPos.x >= border.x || newPos.y < 0 || newPos.y >= border.y) {
	//			float randomAngle = scaleToRange01(random) * 2 * 180;

	//			newPos.x = std::fmin(border.x - 1, std::fmax(0, newPos.x));
	//			newPos.y = std::fmin(border.y - 1, std::fmax(0, newPos.y));
	//			agent.angle = randomAngle;
	//			//agent.rec.setFillColor(sf::Color(scaleToRange01(random) * 255, scaleToRange01(random) * 255, scaleToRange01(random) * 255));
	//			//std::cout << "AgentAngle: " << agent.angle << std::endl;
	//		}

	//		agent.position = newPos;
	//		agent.rec.setPosition(newPos);
	//	}
	//}
	/*std::vector<std::thread> threads;
	for (int i = 0; i < chunks.size(); i++) {
		std::thread thr(&Physarum::updateInParallel, window, chunks[i], deltaTime);
		threads.push_back(thr);
	}

	for (auto& thr : threads) {
		thr.join();
	}*/
}

//void Physarum::updateInParallel(sf::RenderWindow& window, std::vector<Agent>& agentsChunk, sf::Time deltaTime) {
//	auto viewport = window.getView().getViewport();
//	sf::Vector2f border = sf::Vector2f(window.getView().getSize().x, window.getView().getSize().y);
//
//	for (auto& agent : agentsChunk) {
//		auto now = std::chrono::system_clock::now().time_since_epoch();
//		auto count = std::chrono::duration_cast<std::chrono::seconds>(now).count();
//
//		unsigned int random = hash(agent.position.y * border.x + agent.position.x + hash(agent.id + (count + deltaTime.asMilliseconds()) * 100000));
//
//		sf::Vector2f direction = sf::Vector2f(cosf(degreesToRadians(agent.angle)), sin(degreesToRadians(agent.angle)));
//		sf::Vector2f newPos = agent.position + direction * 0.1f * (float)deltaTime.asMilliseconds();
//
//		//std::cout << "NewAgentPos: " << newPos.x << " " << newPos.y << std::endl;
//
//		if (newPos.x < 0 || newPos.x >= border.x || newPos.y < 0 || newPos.y >= border.y) {
//			float randomAngle = scaleToRange01(random) * 2 * 180;
//
//			newPos.x = std::fmin(border.x - 1, std::fmax(0, newPos.x));
//			newPos.y = std::fmin(border.y - 1, std::fmax(0, newPos.y));
//			agent.angle = randomAngle;
//			//agent.rec.setFillColor(sf::Color(scaleToRange01(random) * 255, scaleToRange01(random) * 255, scaleToRange01(random) * 255));
//			//std::cout << "AgentAngle: " << agent.angle << std::endl;
//		}
//
//		agent.position = newPos;
//		agent.rec.setPosition(newPos);
//	}
//}

void Physarum::draw(sf::RenderWindow& window) {

	std::vector<std::future<void>> futures;

	for (auto& chunk : chunks) {
		auto chunkFuture = std::async([&window, &chunk] {
			for (auto& agent : chunk) {
				window.draw(agent.rec);
			}
		});

		futures.push_back(std::move(chunkFuture));
	}

	for (auto& item : futures)
		item.get();

	/*for (auto& chunk : chunks) {
		for (auto& agent : chunk) {
			window.draw(agent.rec);
		}
	}*/
}

