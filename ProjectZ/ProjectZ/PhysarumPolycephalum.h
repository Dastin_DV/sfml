#pragma once
#include <vector>

#include <SFML/System.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics.hpp>

struct Agent {
    sf::RectangleShape rec;
	sf::Vector2f position;
	sf::Color color;
	float angle;
    int id;
};

class Physarum {

public:
	Physarum(const int numAgents);
	void update(sf::RenderWindow& window, sf::Time deltaTime);
	//void updateInParallel(sf::RenderWindow& window, std::vector<Agent>& agentsChunk, sf::Time deltaTime);

	void draw(sf::RenderWindow& window);
    void initAgents();

	const std::vector<std::vector<Agent>>& getChunks() const { return chunks; };
	void setChunks(std::vector<std::vector<Agent>>& chunks) { this->chunks = chunks; };

private:
	std::vector<Agent>* agents;
	std::vector<std::vector<Agent>> chunks;
};