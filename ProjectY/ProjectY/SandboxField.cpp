#include <iostream>

#include "SandboxField.h"

SandboxField::SandboxField(int gridWidth, int gridHeight) : gridWidth(gridWidth), gridHeight(gridHeight) {
	Init();
}

void SandboxField::Init() {

	nodes = new Node[gridWidth * gridHeight];
	shapes = new sf::RectangleShape[gridWidth * gridHeight];

	// Nodes
	for (int x = 0; x < gridWidth; x++) {
		for (int y = 0; y < gridHeight; y++) {
			nodes[y * gridWidth + x].x = x;
			nodes[y * gridWidth + x].y = y;
			nodes[y * gridWidth + x].isObstacle = false;
			nodes[y * gridWidth + x].parent = nullptr;
			nodes[y * gridWidth + x].isVisited = false;
			nodes[y * gridWidth + x].globalGoal = std::numeric_limits<float>::max();
			nodes[y * gridWidth + x].localGoal = std::numeric_limits<float>::max();

			sf::RectangleShape rectangle(sf::Vector2f(nodeSize - nodeBorder, nodeSize - nodeBorder));
			rectangle.setFillColor(sf::Color::Blue);
			rectangle.setPosition(nodeSize * x + nodeBorder, nodeSize * y + nodeBorder);
			std::cout << rectangle.getPosition().x << " " << rectangle.getPosition().y << std::endl;
			Shape newShape;
			newShape.shape = rectangle;
			newShape.state = States::free;

			shapes[y * gridWidth + x] = rectangle;
		}
	}

	// Connections
	for (int x = 0; x < gridWidth; x++) {
		for (int y = 0; y < gridHeight; y++) {
			if (y > 0)
				nodes[y * gridWidth + x].neighbours.push_back(&nodes[(y - 1) * gridWidth + x]);
			if (y < gridHeight - 1)
				nodes[y * gridWidth + x].neighbours.push_back(&nodes[(y + 1) * gridWidth + x]);
			if (x > 0)
				nodes[y * gridWidth + x].neighbours.push_back(&nodes[y * gridWidth + (x - 1)]);
			if (x < gridWidth - 1)
				nodes[y * gridWidth + x].neighbours.push_back(&nodes[y * gridWidth + (x + 1)]);

			//We can also connect diagonally
			if (y>0 && x>0)
				nodes[y* gridWidth + x].neighbours.push_back(&nodes[(y - 1) * gridWidth + (x - 1)]);
			if (y< gridHeight - 1 && x>0)
				nodes[y* gridWidth + x].neighbours.push_back(&nodes[(y + 1) * gridWidth + (x - 1)]);
			if (y>0 && x< gridWidth -1)
				nodes[y* gridWidth + x].neighbours.push_back(&nodes[(y - 1) * gridWidth + (x + 1)]);
			if (y< gridHeight - 1 && x< gridWidth -1)
				nodes[y* gridWidth + x].neighbours.push_back(&nodes[(y + 1) * gridWidth + (x + 1)]);
			
		}
	}

	// Start and end
	startNode = &nodes[gridHeight / 2 * gridWidth + gridWidth / 2];
	startNode->localGoal = 0;
	endNode = &nodes[(gridHeight - 1) * gridWidth + gridWidth - 1];
}

std::pair<int, int> SandboxField::getNodeGridPos(sf::RenderWindow& window) {
	sf::Vector2i pixelPos = sf::Mouse::getPosition(window);
	sf::Vector2f worldPos = window.mapPixelToCoords(pixelPos);

	int selectedNodeGridPosX = worldPos.x / nodeSize;			
	int selectedNodeGridPosY = worldPos.y / nodeSize;

	std::cout << "NodePos " << selectedNodeGridPosX << " " << selectedNodeGridPosY << std::endl;

	if (selectedNodeGridPosX >= 0 && selectedNodeGridPosX < gridWidth) {
		if (selectedNodeGridPosY >= 0 && selectedNodeGridPosY < gridHeight) {
			return { selectedNodeGridPosX, selectedNodeGridPosY };
		}
	}

	return {};
}

void SandboxField::getHeuristic() {

}

void SandboxField::solveAStar() {

	for (int x = 0; x < gridWidth; x++) {
		for (int y = 0; y < gridHeight; y++) {
			nodes[y * gridWidth + x].isVisited = false;
			nodes[y * gridWidth + x].localGoal = INFINITY;
			nodes[y * gridWidth + x].globalGoal = INFINITY;
			nodes[y * gridWidth + x].parent = nullptr;
			nodes[y * gridWidth + x].isPath = false;
		}
	}

	auto distance = [](Node* from, Node* to) {
		return sqrtf((from->x - to->x) * (from->x - to->x) + (from->y - to->y) * (from->y - to->y));
	};

	auto heuristic = [distance](Node* from, Node* to) {
		return distance(from, to);
	};

	std::list<Node*> notTestedNodes;

	startNode->localGoal = 0;
	startNode->globalGoal = heuristic(startNode, endNode);

	Node* currentNode = nullptr;
	notTestedNodes.push_back(startNode);

	while (!notTestedNodes.empty() && currentNode != endNode) {

		notTestedNodes.sort([](const Node* ls, const Node* rs) {
			return ls->globalGoal < rs->globalGoal;
			});

		// Delete visitedNodes from the list
		while (!notTestedNodes.empty() && notTestedNodes.front()->isVisited) {
			notTestedNodes.pop_front();
		}

		if (notTestedNodes.empty())
			break;

		currentNode = notTestedNodes.front();
		currentNode->isVisited = true;

		for (auto neighbour : currentNode->neighbours) {
			if (!neighbour->isVisited && !neighbour->isObstacle) {
				notTestedNodes.push_back(neighbour);
			}

			if (neighbour->localGoal > currentNode->localGoal + distance(currentNode, neighbour)) {
				neighbour->parent = currentNode;
				neighbour->localGoal = currentNode->localGoal + distance(currentNode, neighbour);
				neighbour->globalGoal = neighbour->localGoal + heuristic(currentNode, endNode);
			}
		}
	}
}

void SandboxField::update(sf::Event event, sf::RenderWindow& window) {

	if (event.type == sf::Event::KeyPressed) {
		if (event.key.code == sf::Keyboard::LShift) {
			wasShiftPressed = true;
		}
		if (event.key.code == sf::Keyboard::LControl) {
			wasCntrPressed = true;
		}
	}

	if (event.type == sf::Event::KeyReleased) {
		if (event.key.code == sf::Keyboard::LShift) {
			wasShiftPressed = false;
			std::cout << "ShiftReleased " << std::endl;
		}
		if (event.key.code == sf::Keyboard::LControl) {
			wasCntrPressed = false;
			std::cout << "CntrlReleased " << std::endl;
		}
	}

	if (event.type == sf::Event::MouseButtonReleased) {
		if (event.mouseButton.button == sf::Mouse::Left) {
			auto gridCoordinates = getNodeGridPos(window);
			
			if (wasShiftPressed) {
				startNode = &nodes[gridCoordinates.second * gridWidth + gridCoordinates.first];
			}
			else if (wasCntrPressed) {
				endNode = &nodes[gridCoordinates.second * gridWidth + gridCoordinates.first];
			}
			else {
				nodes[gridCoordinates.second * gridWidth + gridCoordinates.first].isObstacle = !nodes[gridCoordinates.second * gridWidth + gridCoordinates.first].isObstacle;
			}
			solveAStar();
			Node* currentNode = endNode;
			while (currentNode->parent) {
				currentNode->isPath = true;
				currentNode = currentNode->parent;
			}
		}
	}
}

void SandboxField::draw(sf::RenderWindow& window) {

	for (int x = 0; x < gridWidth; x++) {
		for (int y = 0; y < gridHeight; y++) {
			for (auto neigbour : nodes[y * gridWidth + x].neighbours) {

				sf::Vector2f start(x * nodeSize + (nodeSize + nodeBorder) / 2, y * nodeSize + (nodeSize + nodeBorder) / 2);
				sf::Vector2f end(neigbour->x * nodeSize + (nodeSize + nodeBorder) / 2, neigbour->y * nodeSize + (nodeSize + nodeBorder) / 2);
				Line line(start,end,3);

				/*if (neigbour->isPath) {
					line.setColorAll(sf::Color::Yellow);
				}*/
				//window.draw(line, 2, sf::Lines);
				line.draw(window);
			}
		}
	}

	for (int x = 0; x < gridWidth; x++) {
		for (int y = 0; y < gridHeight; y++) {
			if (nodes[y * gridWidth + x].isObstacle) {
				shapes[y * gridWidth + x].setFillColor(sf::Color::Magenta);
				//std::cout << "Obstacle " << std::endl;
			}
			else {
				//std::cout << "Free " << std::endl;
				shapes[y * gridWidth + x].setFillColor(sf::Color::Blue);
			}

			if (nodes[y * gridWidth + x].isVisited) {
				shapes[y * gridWidth + x].setFillColor(sf::Color(135, 206, 250));
				//std::cout << "Obstacle " << std::endl;
			}

			if (&nodes[y * gridWidth + x] == startNode) {
				//std::cout << "StartNode " << std::endl;
				shapes[y * gridWidth + x].setFillColor(sf::Color::Green);
			}
			if (&nodes[y * gridWidth + x] == endNode) {
				//std::cout << "EndNode " << std::endl;
				shapes[y * gridWidth + x].setFillColor(sf::Color::Red);
			}
			window.draw(shapes[y * gridWidth + x]);
		}
	}

	if (endNode != nullptr)
	{
		Node* p = endNode;
		while (p->parent != nullptr)
		{
			sf::Vector2f start(p->x * nodeSize + (nodeSize + nodeBorder) / 2, p->y * nodeSize + (nodeSize + nodeBorder) / 2);
			sf::Vector2f end(p->parent->x * nodeSize + (nodeSize + nodeBorder) / 2, p->parent->y * nodeSize + (nodeSize + nodeBorder) / 2);
			Line line(start, end, 7);
			line.setColorAll(sf::Color::Yellow);
			line.draw(window);
			p = p->parent;
		}
	}
}