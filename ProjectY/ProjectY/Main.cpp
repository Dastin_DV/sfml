#include <SFML/Graphics.hpp>

#include "SandboxField.h"

int main()
{
    sf::RenderWindow window(sf::VideoMode(1000, 1000), "SFML works!");
    sf::CircleShape shape(100.f);
    shape.setFillColor(sf::Color::Green);

    SandboxField sandbox(20, 20);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            sandbox.update(event, window);
        }

        window.clear();
        sandbox.draw(window);
        window.display();
    }

    return 0;
}