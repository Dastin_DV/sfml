#pragma once
#include <vector>
#include <list>
#include "SFML/Graphics.hpp"

// GraphNode
struct Node {
	bool isObstacle = false;
	bool isVisited = false;
	bool isPath = false;
	float globalGoal;
	float localGoal;
	int x, y;
	std::vector<Node*> neighbours;
	Node* parent;
};

enum class States {
	start,
	end,
	obstacle,
	free
};

struct Shape {
	sf::RectangleShape shape;
	States state;
};

class Line
{
public:
    sf::Vector2f a, b;
    sf::ConvexShape ln;
    sf::CircleShape A, B;
    Line(sf::Vector2f ia, sf::Vector2f ib, int width)
        : a(ia), b(ib)
    {
        float length = sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2));
        float rot = atan2(b.x - a.x, b.y - a.y);
        float X1, Y1, X2, Y2;
        X1 = sin(rot + 1.570796) * (width / 2); // + 90�
        Y1 = cos(rot + 1.570796) * (width / 2); // + 90�
        X2 = sin(rot + 4.712388) * (width / 2); // + 270�
        Y2 = cos(rot + 4.712388) * (width / 2); // + 270�

        A = sf::CircleShape(width / 2, 20);
        B = sf::CircleShape(width / 2, 20);
        A.setPosition(ia.x - width / 2, ia.y - width / 2);
        B.setPosition(ib.x - width / 2, ib.y - width / 2);
        A.setFillColor(sf::Color(255, 255, 255));
        A.setFillColor(sf::Color(255, 255, 255));

        ln.setPointCount(4);
        ln.setPoint(0, ia + sf::Vector2f(X1, Y1));
        ln.setPoint(1, ia + sf::Vector2f(X2, Y2));
        ln.setPoint(2, ib + sf::Vector2f(X2, Y2));
        ln.setPoint(3, ib + sf::Vector2f(X1, Y1));
        ln.setFillColor(sf::Color(255, 255, 255));
    };
    void setColorAll(sf::Color col)
    {
        A.setFillColor(col);
        B.setFillColor(col);
        ln.setFillColor(col);
    };
    void draw(sf::RenderWindow& win)
    {
        (win).draw(ln);
        (win).draw(A);
        (win).draw(B);
    };
};

class SandboxField {

public:
	SandboxField(int gridWidth, int gridHeight);
	void Init();

	void update(sf::Event event, sf::RenderWindow& window);
	void draw(sf::RenderWindow& window);

private:

	Node* nodes = nullptr;
	Node* startNode = nullptr;
	Node* endNode = nullptr;

	sf::RectangleShape* shapes = nullptr;
	std::pair<int, int> getNodeGridPos(sf::RenderWindow& window);
	void solveAStar();
	void getHeuristic();

	bool wasShiftPressed = false;
	bool wasCntrPressed = false;
	std::pair<int, int> lastClickedEvent;
	int gridHeight = 16;
	int gridWidth = 16; 
	int nodeSize = 40;
	int nodeBorder = 20;
};
